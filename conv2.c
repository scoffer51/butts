/*
 ============================================================================
 Name        : conv2.c
 Author      : Stenyaev A.
 Version     : rel.03.05.2019
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
///
#include <windows.h>
#include <string.h>
#include <sys/types.h>
#include <stdbool.h>
#include <winbase.h>
///
#include <sys/time.h>
#include <time.h>
#include <winnt.h>
#include <math.h>
///
#define visualization_of_intermediate_calculations_when_debugging
//#undef visualization_of_intermediate_calculations_when_debugging
#define visualization_of_intermediate_calculations_when_debugging_extrapolation
//#undef visualization_of_intermediate_calculations_when_debugging_extrapolation
///
#define Gx "1FFF409"
#define sbits_Gx "0001111111111111010000001001"
#define no_parity "000000"
#define symbol "0123456789ABCDEF"
#define Mx "8D43BE895829014E2CD996"
#define Mx0 "8D43BE895827715062DC70"
#define Mx1 "8D43BE89582764B130A736"
#define Mx2 "8D43BE8999C0960EE83085"
#define Mx3 "8D43BE89582764B126A745"
#define Mx4 "8D43BE89582751506CDC97"
#define Mx5 "8D43BE89582751508CDCBC"
#define Mx6 "8D43BE8958235155D2E1DF"
#define Mx7 "8D43BE8999C0970EE83C86"
#define Mx8 "8D43BE8958235155D2E1DF"
#define Mx9 "8D43BE89582354B69AAC88"
#define Mx10 "8D43BE89582344B6A4AC8E"
#define Mx11 "8D43BE895823415600E1FC"
#define Mx12 "8D43BE8999C0980EC84086"
#define Mx13 "8D43BE895823415600E1FC"
#define Mx14 "8D43BE89582334B6CCACA7"
#define Mx15 "8D43BE8999C0990EA84486"
#define Mx16 "8D43BE89582314B6F8ACC2"

#define event 19
#define positions 17
#define clones 10
#define phantoms 19
#define approximation 34
#define ring 60
///
#define bits_CRC 24
#define FT 0.3048
#define KT 0.51444
#define FPM 196.85
#define RAD PI/180
#define q17 pow(2, 17)
#define ODD 360/59
#define EVEN 360/60
#define cNZ 15
#define R 6378.137
#define sj 1 / 298.257223563
#define PI 3.14159265358979
#define tolerance 0.0000000000000001
///
#define AP "43BE89"
///
// ...	������ ��� �������������
typedef struct _DATAFOREXT {
	double dLAT[event + 5];
	double dLON[event + 5];
	double ALT_m[event + 5];
	double spd_m_s[2][event + 5];
	double spd_sn_m_s[2][event + 5];
	double spd_we_m_s[2][event + 5];
	double climb_m_s[2][event + 5];
	double angle_deg[2][event + 5];
	double interval[event + 5];
	int index[3];
	int event_log[event + 5];					// ������ �������
	int iframe[3][event + 5];
	int global[event + 5];
} DATAFOREXT, *LPDATAFOREXT;
///
// ...	������������ ������ � ������������� ������� ��������� (DSC)
typedef struct _SDATA {
	double X[positions];
	double Y[positions];
	double Z[positions];
	double N[positions];
	double rLAT[positions];
	double rLON[positions];
	double cosB[positions];
	double sinB[positions];
	double cosL[positions];
	double sinL[positions];
	double cosBsinL[positions];
	double cosBcosL[positions];
	double sinBsinL[positions];
	double sinBcosL[positions];
	double distance[positions];
	double step_time[positions];
	int event_pos[positions];
} SDATA, *LPSDATA;
///
// ...	��������� ������� ��������� � �������������� �������
//		(source_data_airborne_positions)
typedef struct _SDAP {
	char hData[112*4][2];
    int DF_dec;
    int CA_dec;
    int Address[5];
    int TC_dec;
    int SS_dec;
    int NICsb;
    int Q_bit;
    int T;
    int alt_dec;
    int lat_dec;
    int lon_dec;
} SDAP, *LPSDAP;
///
// ...	��������� ������������� ��������� CPR
//		(data_representation_of_coordinates)
typedef struct _DRC {
	double LAT[4];
    double LON[4];
    int ALT_ft[3];
    double Lat_EVEN;
    double Lat_ODD;
    double CPR_LAT;
    double CPR_LAT_EVEN;
    double CPR_LON;
    double CPR_LON_EVEN;
    double CPR_LAT_ODD;
    double CPR_LON_ODD;
    int j;
    int m;
    int N_dec[2];
    int Q_bit[2];
} DRC, *LPDRC;
///
// ...	��������� ������������ ������� �������������������
typedef struct _BITSQ {
	int numb;
	int tetra[4];
	int bitsGx[25];
	int bitsMx[88];
	int bitsAP[24];
	int bitsPx[28];
} BITSQ, *LPBITSQ;
///
typedef struct _BITSG {
	int bitsGx[28];
	int bitsMx[112];
	int frame[112];
	int bitsAP[24];
} BITSG, *LPBITSG;
///
typedef struct _BASEFRAME {
	int DF[5];
	int CA[3];
	int ICAO[24];
	int TC[5];
	int DF_DEC;
	int CA_DEC;
	int ADDRESS_DEC;
	int TC_DEC;
	int previous_value_F[3];
} BASEFRAME, *LPBASEFRAME;
///
typedef struct _POZFRAME {
	int bit48__Q_bit;
	int bit53__T;
	int bit54__F;
	int SS[2];
	int NICsb;
	int ALT[11];
	int CPR_LAT[17];
	int CPR_LON[17];
	int SS_DEC;
	int ALT_DEC;
	int ALT_ft;
	double ALT_m;
	int CPR_LAT_DEC;
	int CPR_LON_DEC;
} POZFRAME, *LPPOZFRAME;
///
typedef struct _PREVPOS {
	double ALT_m;
	int CPR_LAT_DEC;
	int CPR_LON_DEC;
} PREVPOS, *LPPREVPOS;
///
typedef struct _SPDFRAME {
	int ST[3];
	int bit41__IC;
	int bit42__RESV_A;
	int NAC[3];
	int bit46__S_ew;
	int V_ew[10];
	int bit57__S_ns;
	int V_ns[10];
	int bit68__VrSrc;
	int bit69__S_vr;
	int Vr[9];
	int RESV_B[2];
	int bit81__S_Dif;
	int Dif[7];
	int ST_DEC;
	int NAC_DEC;
	int V_ew_DEC;
	int V_ns_DEC;
	int Vr_DEC;
	int RESV_B_DEC;
	int Dif_DEC;
} SPDFRAME, *LPSPDFRAME;
///
typedef struct _SPEEDPAR {
	int Vwe_kt;
	int Vsn_kt;
	double Vgr_kt;
	double h_deg[2];
	int climb_fpm;
	int Vair_kt;
	double Vwe_m_s;
	double Vsn_m_s;
	double Vgr_m_s;
	double climb_m_s;
	double Vair_m_s;
	double h_rad;
} SPEEDPAR, *LPSPEEDPAR;
///
///
typedef struct _CPRCALCGLB {
	double CPR_LAT_EVEN;
	double CPR_LON_EVEN;
	double CPR_LAT_ODD;
	double CPR_LON_ODD;
	double LAT_EVEN;
	double LAT_ODD;
	double LON_EVEN;
	double LON_ODD;
	double LAT;
	double LON;
	double dLAT;
	double dLON[2];
	int F[2];
	int J;
	int mod_even[2];
	int mod_odd[2];
	int NZ;
	int NL[2];
	int ni[2];
	int m[2];
} CPRCALCGLB, *LPCPRCALCGLB;
///
typedef struct _CPRCALCLOC {
	double CPR_LAT;
	double CPR_LON;
	double LAT;
	double LON;
	double LAT_ref;
	double LON_ref;
	double dLAT;
	double dLON;
	int F;
	int J;
	double mod[2];
	int NZ;
	int NL;
	int m;
} CPRCALCLOC, *LPCPRCALCLOC;
///
// ...	��������� ��� ���������� ��������������
//		(�������������) �������� ���������� ����������
// 		(intermediate_values_speeds)
typedef struct _DATAINTERVAL {
	double sp1;
	double sp2;
	double sp21;
	double crd0;
	double crd1;
	double crd2;
	double crd3;
	double crd21;
	double crd31;
	int arrayS[positions];
	int index[4];
} DATAINTERVAL, *LPDATAINTERVAL;
///
// ...	��������� ������ ��� ����������������� ����� ��������� ���������
//		(primary_data_for_extrapolation)
typedef struct _DATAPRIMARY {
	double loc[3][positions];
	double spd[3][positions];
	//double mov[3][positions];
	double dist[2][event];
	double Vgr[2][event];
	double dt[2][event];
	double acc[3][event];
	double t[2][event];
	int point;
} DATAPRIMARY, *LPDATAPRIMARY;
///
///
typedef struct _DMAT {
	double a[3];
	double b[3];
    double c[3];
    double d[3];
    double det[4];
    double R1[approximation];
    double R2[approximation];
    double R3[approximation];
    BOOL mode[2];
} DMAT, *LPDMAT;
///
typedef struct _DDE {
	double ti[approximation];
	double st[approximation];
	double sP[approximation];
    double rst2[approximation];
    double stP[approximation];
    double rst3[approximation];
    double rst4[approximation];
    double sPrt2[approximation];
} DDE, *LPDDE;
///
typedef struct _CIRCLE {
	double Xo[2];
	double Zo[2];
	double radius_km[2];
	int point[2];
	double x[2][ring];
	double z[2][ring];
} CIRCLE, *LPCIRCLE;
///
typedef struct _RANDNUMBS {
	int numb1[phantoms];
	int numb2[phantoms];
	int mode1[phantoms];
	int mode2[phantoms];
} RANDNUMBS, *LPRANDNUMBS;
///
typedef struct _POINT0 {
	double Bo[clones];
	double Lo[clones];
	double Ho[clones];
	double X0[clones];
	double Z0[clones];
	double Y0[clones];
	double time_tsc;
} POINT0, *LPPOINT0;
///
typedef struct _POINTK {
	double Bk[clones][positions];
	double Lk[clones][positions];
	double Hk[clones][positions];
	double Xk[clones][positions];
	double Zk[clones][positions];
	double Yk[clones][positions];
	double tk[clones][positions];
	double deviation_m[clones][positions];
	double deviation0;
	double distPo[clones];
	double distPk;
} POINTK, *LPPOINTK;
///

DATAFOREXT dfex;
SDATA sdat;
SDAP sdp;
DRC dcpr;
BITSQ qbit;
BITSG qbit1;
BITSQ qbit2;
BITSQ qbit3;
BASEFRAME bfrm;
POZFRAME poz;
PREVPOS prev;
SPDFRAME spd;
SPEEDPAR pspd;
CPRCALCGLB cprg;
CPRCALCLOC cprl;
DATAINTERVAL sval;
DATAPRIMARY dprm;
///
DMAT dMat;
DDE dde;
CIRCLE crcl;
RANDNUMBS rnd;
POINT0 p0;
POINTK pk;

///
BOOL bmode, bAP, bCRC_clone;
BOOL bpTSC, bfirst;
int session, bits_data, jmode, hpos, ishift, jcount, bits_Gx;
int jShow, jj, jd, jd1, jsc, jres, count;
int CRC24_dec;
double aW, e2;
int pos;
char *parity, *frame, *sGx, *sbGx, *sSymb, *sICAO, *Dcrc;
///
double _B1, _B2, _B3, _C1, _C2, _C3, _A1, _A2, _A3, _D1, _D2, _D3;
double __G1, __G2, __G3, __G4, __G5, __G6;
double __H1, __H2, __H3, __H4, __H5, __H6;
double __J1, __J2, __J3, __J4, __J5, __J6;
double __K1, __K2, __K3, __K4, __K5, __K6;
double __G11, __G21, __G31, __G41, __G51, __G61;
double __G12, __G22, __G32, __G42, __G52, __G62;
double __G13, __G23, __G33, __G43, __G53, __G63;

///
// ...	��������� �������� (�������)
int calculating_frame_parity(int bits_data, BOOL bAP, BOOL bCRC_clone, int check);
int convertion_to_decimal(int numb_dec, int rank);
int convert_data_HexToBits(int jcount, int hpos, int ishift, int jmode, BOOL bmode);
int convert_data_HexToBits1(int jcount, int hpos, int ishift, int jmode, BOOL bmode);
int conv_half_byte(int numeric);
int decomp_frames_into_components(int mode_frame, int check);
int coding_DEC_format_glob(int CPR_LAT_DEC1, int CPR_LON_DEC1, int CPR_LAT_DEC2, int CPR_LON_DEC2, int bit54);
int calc_NL_glb(double cLAT);
int calc_m_glb(int CPR_LON1, int CPR_LON2, BOOL bm);

int coding_DEC_format_local(int CPR_LAT_DEC1, int CPR_LON_DEC1, int bit54, double latExt, double lonExt);
int calc_NL_loc(double RLat);

int encoding_CPR_format(BOOL bGlob, int check, int bit54, double latExt, double lonExt);
int message_handler(int check);
int convert_to_DSC(int ibeg, int numb);
double radians(double degress);
int frame_simulator(int check_frames);
int calculating_intermediate_values_speeds(int modeS);
int index_detection(int indexP);
int distance_calculation_plus(int countD, int modeD);
///
int determinant_matrix(int check, int module, BOOL bmode);
double calc_distance_between_two_points(int ip1, int ip2);
double time_interval_calculation(int ip1, int ip2);
int calculation_of_approximation_coefficients(int clone, int check, int module);
int initialization_of_flight_path_source_data(int clone, int points, double medium_s);
double saving_coordinates_as_point_by_trajectory(int clone, int index, int pos, BOOL bpTSC);


///
/// ...	���������� ����������� ����� CRC24 frame
int calculating_frame_parity(int bits_data, BOOL bAP, BOOL bCRC_clone, int numbF) {
	///
	jd = 0;
	///
	if (bAP) {
		// ...	��������������� CRC24 �������� ��������� � ������� ������������������
		qbit.numb = 3;
		count = strlen(AP);
		convert_data_HexToBits(count, hpos, ishift, jmode, bmode);
	}
	///
	qbit.numb = 2;
	convert_data_HexToBits1(count, hpos, ishift, jmode, bmode);
	///
	int jp, jr;
	BOOL _flag = false;
	jd = 0;
	// ...
	if (numbF == 0) {
		for (jp = 0; jp < bits_Gx; jp++) {
			qbit.bitsGx[jp] =  qbit.bitsPx[jp + 3];
#ifdef visualization_of_intermediate_calculations_when_debugging
			jd++;
			if (jd == 4) {
				printf("%d ", qbit.bitsGx[jp]);
				jd = 0;
			}
			else {
				if (jp == 0)
						printf("\nGx: %d", qbit.bitsGx[jp]);
				else	printf("%d", qbit.bitsGx[jp]);
			}
#endif
		}
	}
	else {
		for (jj = 0; jj < bits_data + bits_CRC; jj++) qbit1.bitsMx[jj] = 0;
		for (jr = 0; jr < bits_CRC; jr++) qbit1.bitsAP[jr] = 0;
	}
	///
	//Dcrc = malloc(sizeof(char) * 6 + 1);
	//strcpy(Dcrc, no_parity);
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("\nDcrc: %s\t bits_Gx: %d\t bits_Mx: %d\t bits_CRC: %d\n", Dcrc, bits_Gx, bits_data, bits_CRC);
#endif
	///
	for (jj = 0; jj < bits_data; jj++) qbit1.bitsMx[jj] = qbit2.bitsMx[jj];
#ifndef visualization_of_intermediate_calculations_when_debugging
	puts("�������� ������������������ (frame) ��� CRC24:");
	for (jsc = 0; jsc < bits_data + bits_CRC; jsc++) {
		if (jd == 4) {
				printf("%d ", qbit1.bitsMx[jsc]);
				jd = 0;
		}
		else 	printf("%d", qbit1.bitsMx[jsc]);
		jd++;
	}
	puts("\n");
#endif
	///
	for (jj = 0; jj < bits_data; jj++) {
		if (qbit1.bitsMx[jj] == 1) {
			for (jp = 0; jp < bits_Gx; jp++) {
				if (qbit1.bitsMx[jj + jp] ==  qbit.bitsGx[jp])
						qbit1.bitsMx[jj + jp] = 0;
				else	qbit1.bitsMx[jj + jp] = 1;
#ifndef visualization_of_intermediate_calculations_when_debugging
				if (jj == 0) {
					jd1 = 0;
					for (jsc = 0; jsc < bits_data + bits_CRC; jsc++) {
						jd1++;
						if (jd1 == 4) {
								printf("%d ", qbit1.bitsMx[jsc]);
								jd1 = 0;
						}
						else 	printf("%d", qbit1.bitsMx[jsc]);
					}
					puts("\n");
				}
#endif
			}
			_flag = true;
			///
			if ((jj >= bits_data - bits_Gx) && (_flag)) {
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("%d\t", jj);
#endif
				jd1 = 0;
				for (jr = 0; jr < bits_Gx; jr++) {
					qbit1.bitsAP[jr] = qbit1.bitsMx[bits_data + jr];
#ifndef visualization_of_intermediate_calculations_when_debugging
					jd1++;
					if (jr < bits_CRC - 1) {
						if (jd1 == 4) {
								printf("%d ", qbit1.bitsAP[jr]);
								jd1 = 0;
						}
						else	printf("%d", qbit1.bitsAP[jr]);
					}
					else	printf("%d\n", qbit1.bitsAP[jr]);
#endif
				}
				_flag = false;
			}
		}
	}
	///
	// ...	�������������� (�������������) ����������� �������� frame
	for (jj = 0; jj < bits_data; jj++) qbit1.bitsMx[jj] = qbit2.bitsMx[jj];
	qbit.numb = 0;
#ifndef visualization_of_intermediate_calculations_when_debugging
	if (qbit.numb == 0) count = count + 6;
	printf("\nbits: %d\n", count * 4);
	jd = 0;
	for (jj = 0; jj < count * 4; jj++) {
		jd++;
		if (jd == 4) {
			if (qbit.numb == 0) printf("%d ", qbit1.bitsMx[jj]);
			if (qbit.numb == 1) printf("%d ", qbit1.bitsGx[jj]);
			else if (qbit.numb == 2) printf("%d ", qbit2.bitsMx[jj]);
			else if (qbit.numb == 3) printf("%d ", qbit3.bitsAP[jj]);
			jd = 0;
		}
		else {
			if (qbit.numb == 0) printf("%d", qbit1.bitsMx[jj]);
			if (qbit.numb == 1)	printf("%d", qbit1.bitsGx[jj]);
			else if (qbit.numb == 2)	printf("%d", qbit2.bitsMx[jj]);
			else if (qbit.numb == 3)	printf("%d", qbit3.bitsAP[jj]);
		}
	}
	puts("\n");
#endif
	///
    // ...	��������������� CRC24 � ���������� ��� (numb_dec = 1)
	return convertion_to_decimal(1, bits_CRC);
}
///
/// ...	��������������� � ���������� ���
int convertion_to_decimal(int numb_dec, int rank) {
	int rb, result = 0;
    ///
    for (int js = 0; js < rank; js++)
    {
    	if (numb_dec == 1) rb = qbit1.bitsAP[js];
    	if (numb_dec == 2) rb = bfrm.DF[js];
    	if (numb_dec == 3) rb = bfrm.CA[js];
    	if (numb_dec == 4) rb = bfrm.ICAO[js];
    	if (numb_dec == 5) rb = bfrm.TC[js];
    	if (numb_dec == 6) rb = poz.SS[js];
    	if (numb_dec == 7) rb = poz.ALT[js];
    	if (numb_dec == 8) rb = poz.CPR_LAT[js];
    	if (numb_dec == 9) rb = poz.CPR_LON[js];
    	if (numb_dec == 10) rb = spd.V_ew[js];
    	if (numb_dec == 11) rb = spd.V_ns[js];
    	if (numb_dec == 12) rb = spd.Vr[js];
    	if (numb_dec == 13) rb = spd.Dif[js];
    	if (numb_dec == 14) rb = spd.ST[js];
    	if (numb_dec == 15) rb = spd.NAC[js];
    	if (numb_dec == 16) rb = spd.RESV_B[js];
        result = result + rb * pow(2, rank - js - 1);
    }
	return result;
}
///������� ������������������
/// ...
int convert_data_HexToBits(int jcount, int hpos, int ishift, int jmode, BOOL bmode) {
	///
	jj = 0;
	///
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("numb: %d\tGx: ", qbit.numb);
#endif
	char *str = malloc(sizeof(char) * jcount);
	///
	for (jd = 0; jd < jcount; jd++) {
		switch (qbit.numb) {
		case 1:
			strcpy(str, sGx);
			memset(sdp.hData[jd], str[jd], 1);
#ifdef visualization_of_intermediate_calculations_when_debugging
			printf("%s", sdp.hData[jd]);
#endif
			break;
		case 2:
			strcpy(str, frame);
			memset(sdp.hData[jd], str[jd], 1);
#ifdef visualization_of_intermediate_calculations_when_debugging
			printf("%s", sdp.hData[jd]);
#endif
			break;
		case 3:
			strcpy(str, sICAO);
			memset(sdp.hData[jd], str[jd], 1);
#ifdef visualization_of_intermediate_calculations_when_debugging
			printf("%s", sdp.hData[jd]);
#endif
			break;
		default:
				sdp.hData[jd + hpos][0] = 0;
		}
		///
		for (jsc = 0; jsc < 16; jsc++) {
			if (str[jd] == sSymb[jsc]) {
				jres = jsc;
				break;
			}
		}
		free(str);
		///
#ifndef visualization_of_intermediate_calculations_when_debugging
		printf("\t%d\n", jres);
#endif
		// ...	������������ ��������� (��������������� HEX � ��������)
		conv_half_byte(jres);
		///
		for (int jw = 0; jw < 4; jw++) {
			if (qbit.numb == 1) qbit1.bitsGx[jj + jw] = qbit.tetra[jw];
			else if (qbit.numb == 2) qbit2.bitsMx[jj + jw] = qbit.tetra[jw];
			else if (qbit.numb == 3) qbit3.bitsAP[jj + jw] = qbit.tetra[jw];
		}
		///
		jj = jj + 4;
	}
	///
#ifndef visualization_of_intermediate_calculations_when_debugging
	jd = 0;
	for (jj = 0; jj < jcount * 4; jj++) {
		jd++;
		if (jd == 4) {
			if (qbit.numb == 1) 		printf("%d ", qbit1.bitsGx[jj]);
			else if (qbit.numb == 2) 	printf("%d ", qbit2.bitsMx[jj]);
			else if (qbit.numb == 3) 	printf("%d ", qbit3.bitsAP[jj]);
			jd = 0;
		}
		else {
			if (qbit.numb == 1) 		printf("%d", qbit1.bitsGx[jj]);
			else if (qbit.numb == 2) 	printf("%d", qbit2.bitsMx[jj]);
			else if (qbit.numb == 3) 	printf("%d", qbit3.bitsAP[jj]);
		}
	}
	puts("\n");
#endif
	///
	return jcount;
}
///
int convert_data_HexToBits1(int jcount, int hpos, int ishift, int jmode, BOOL bmode) {
	///
	jj = 0;
	///
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("numb: %d\tGx: ", qbit.numb);
#endif
	char *str = malloc(sizeof(char) * jcount);
	///
	for (jd = 0; jd < jcount; jd++) {
		switch (qbit.numb) {
		case 1:
			strcpy(str, sGx);
			memset(sdp.hData[jd], str[jd], 1);
#ifdef visualization_of_intermediate_calculations_when_debugging
			printf("%s", sdp.hData[jd]);
#endif
			break;
		case 2:
			strcpy(str, frame);
			memset(sdp.hData[jd], str[jd], 1);
#ifdef visualization_of_intermediate_calculations_when_debugging
			printf("%s", sdp.hData[jd]);
#endif
			break;
		case 3:
			strcpy(str, sICAO);
			memset(sdp.hData[jd], str[jd], 1);
#ifdef visualization_of_intermediate_calculations_when_debugging
			printf("%s", sdp.hData[jd]);
#endif
			break;
		default:
				sdp.hData[jd + hpos][0] = 0;
		}
		///
		for (jsc = 0; jsc < 16; jsc++) {
			if (str[jd] == sSymb[jsc]) {
				jres = jsc;
				break;
			}
		}
		///
#ifndef visualization_of_intermediate_calculations_when_debugging
		printf("\t%d\n", jres);
#endif
		// ...	������������ ��������� (��������������� HEX � ��������)
		conv_half_byte(jres);
		for (int jw = 0; jw < 4; jw++) {
			if (qbit.numb == 1) qbit1.bitsGx[jj + jw] = qbit.tetra[jw];
			else if (qbit.numb == 2) qbit2.bitsMx[jj + jw] = qbit.tetra[jw];
			else if (qbit.numb == 3) qbit3.bitsAP[jj + jw] = qbit.tetra[jw];
		}
		///
		jj = jj + 4;
	}
	free(str);
	///
#ifndef visualization_of_intermediate_calculations_when_debugging
	jd = 0;
	for (jj = 0; jj < jcount * 4; jj++) {
		jd++;
		if (jd == 4) {
			if (qbit.numb == 1) 		printf("%d ", qbit1.bitsGx[jj]);
			else if (qbit.numb == 2) 	printf("%d ", qbit2.bitsMx[jj]);
			else if (qbit.numb == 3) 	printf("%d ", qbit3.bitsAP[jj]);
			jd = 0;
		}
		else {
			if (qbit.numb == 1) 		printf("%d", qbit1.bitsGx[jj]);
			else if (qbit.numb == 2) 	printf("%d", qbit2.bitsMx[jj]);
			else if (qbit.numb == 3) 	printf("%d", qbit3.bitsAP[jj]);
		}
	}
	puts("\n");
#endif
	///
	return jcount;
}
///
/// ...	������������ ��������� (��������������� HEX � ��������)
int conv_half_byte(int numeric) {
	switch (numeric) {
	case 1:
		qbit.tetra[0] = qbit.tetra[1] =	qbit.tetra[2] = 0;
		qbit.tetra[3] = 1;
		break;
	case 2:
		qbit.tetra[0] = qbit.tetra[1] =	qbit.tetra[3] = 0;
		qbit.tetra[2] = 1;
		break;
	case 3:
		qbit.tetra[0] = qbit.tetra[1] = 0;
		qbit.tetra[2] =	qbit.tetra[3] = 1;
		break;
	case 4:
		qbit.tetra[0] = qbit.tetra[2] =	qbit.tetra[3] = 0;
		qbit.tetra[1] = 1;
		break;
	case 5:
		qbit.tetra[0] = qbit.tetra[2] = 0;
		qbit.tetra[1] =	qbit.tetra[3] = 1;
		break;
	case 6:
		qbit.tetra[0] = qbit.tetra[3] = 0;
		qbit.tetra[1] =	qbit.tetra[2] = 1;
		break;
	case 7:
		qbit.tetra[0] = 0;
		qbit.tetra[1] = qbit.tetra[2] =	qbit.tetra[3] = 1;
		break;
	case 8:
		qbit.tetra[0] = 1;
		qbit.tetra[1] = qbit.tetra[2] =	qbit.tetra[3] = 0;
		break;
	case 9:
		qbit.tetra[0] = qbit.tetra[3] =	1;
		qbit.tetra[1] = qbit.tetra[2] = 0;
		break;
	case 10:
		qbit.tetra[0] = qbit.tetra[2] =	1;
		qbit.tetra[1] = qbit.tetra[3] = 0;
		break;
	case 11:
		qbit.tetra[0] = qbit.tetra[2] =	qbit.tetra[3] = 1;
		qbit.tetra[1] = 0;
		break;
	case 12:
		qbit.tetra[0] = qbit.tetra[1] =	1;
		qbit.tetra[3] = qbit.tetra[2] = 0;
		break;
	case 13:
		qbit.tetra[0] = qbit.tetra[1] =	qbit.tetra[3] = 1;
		qbit.tetra[2] = 0;
		break;
	case 14:
		qbit.tetra[0] = qbit.tetra[1] =	qbit.tetra[2] = 1;
		qbit.tetra[3] = 0;
		break;
	case 15:
		qbit.tetra[0] = qbit.tetra[1] =	qbit.tetra[2] = qbit.tetra[3] = 1;
		break;
	default:
		qbit.tetra[0] = qbit.tetra[1] =	qbit.tetra[2] = qbit.tetra[3] = 0;
	}
	return numeric;
}
///
/// ...	���������� �� ���������� �������� ��������� (��� �����)
int decomp_frames_into_components(int mode_frame, int check) {
	int jscfr;
	if (mode_frame == 1) {
		for (jscfr = 0; jscfr < 5; jscfr++) {
			bfrm.DF[jscfr] = qbit1.frame[jscfr];
			if (jscfr < 3) bfrm.CA[jscfr] = qbit1.frame[5 + jscfr];
			bfrm.TC[jscfr] = qbit1.frame[32 + jscfr];
#ifndef visualization_of_intermediate_calculations_when_debugging
			printf("%d", bfrm.DF[jscfr]);
#endif
		}
		for (jscfr = 0; jscfr < 24; jscfr++) {
			bfrm.ICAO[jscfr] = qbit1.frame[8 + jscfr];
#ifndef visualization_of_intermediate_calculations_when_debugging
			printf("%d", bfrm.ICAO[jscfr]);
#endif
		}
		///
		bfrm.DF_DEC = convertion_to_decimal(2, 5);
		bfrm.CA_DEC = convertion_to_decimal(3, 3);
		bfrm.ADDRESS_DEC = convertion_to_decimal(4, 24);
#ifdef visualization_of_intermediate_calculations_when_debugging
		printf("# DF: %d  CA: %d\t\t Address: %d\n", bfrm.DF_DEC, bfrm.CA_DEC, bfrm.ADDRESS_DEC);
#endif
		///
		if (bfrm.DF_DEC == 17) {
			///
			bfrm.TC_DEC = convertion_to_decimal(5, 5);
			if (bfrm.TC_DEC == 11) {
				dfex.event_log[check] = 0;
				pos++;
				// ...	frame '�������������� �������'
				poz.SS[0] = qbit1.frame[37];
				poz.SS[1] = qbit1.frame[38];
				poz.SS_DEC = convertion_to_decimal(6, 2);
				poz.NICsb = qbit1.frame[39];
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("# TC: %d  SS: %d\t\t NICsb: %d\n", bfrm.TC_DEC, poz.SS_DEC, poz.NICsb);
#endif
				///
				for (jscfr = 0; jscfr < 12; jscfr++) {
					if (jscfr < 7) poz.ALT[jscfr] = qbit1.frame[40 + jscfr];
					else if (jscfr > 7) poz.ALT[jscfr - 1] = qbit1.frame[40 + jscfr];
					else if (jscfr == 7) poz.bit48__Q_bit = qbit1.frame[40 + jscfr];
#ifndef visualization_of_intermediate_calculations_when_debugging
					if (jscfr < 7) printf("%d", poz.ALT[jscfr]);
					if (jscfr > 7) printf("%d", poz.ALT[jscfr - 1]);
#endif
				}
				// ...	���������� ��� ��������� 'ALT'
				poz.ALT_DEC = convertion_to_decimal(7, 11);
				// ...	������ � ����� (ft)
				if (poz.bit48__Q_bit == 0)
						poz.ALT_ft = poz.ALT_DEC * 100 - 1000;
				else	poz.ALT_ft = poz.ALT_DEC * 25 - 1000;
				// ...	������ � ������ (m)
				poz.ALT_m = poz.ALT_ft * FT;
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("# ALT (code): %d\t Q-bit: %d ALT: %d ft = %f m\n", poz.ALT_DEC, poz.bit48__Q_bit, poz.ALT_ft, poz.ALT_m);
#endif
				///
				for (jscfr = 0; jscfr < 17; jscfr++) {
					poz.CPR_LAT[jscfr] = qbit1.frame[54 + jscfr];
#ifndef visualization_of_intermediate_calculations_when_debugging
					printf("%d", poz.CPR_LAT[jscfr]);
#endif
				}
				for (jscfr = 0; jscfr < 17; jscfr++) {
					poz.CPR_LON[jscfr] = qbit1.frame[71 + jscfr];
#ifndef visualization_of_intermediate_calculations_when_debugging
					printf("%d", poz.CPR_LON[jscfr]);
#endif
				}
				///
				poz.bit53__T = qbit1.frame[52];
				poz.bit54__F = qbit1.frame[53];
				poz.CPR_LAT_DEC = convertion_to_decimal(8, 17);
				poz.CPR_LON_DEC = convertion_to_decimal(9, 17);
#ifdef visualization_of_intermediate_calculations_when_debugging
				printf("# F: %d\n# CPR_LAT (code): %d\n# CPR_LON (code): %d\n", poz.bit54__F, poz.CPR_LAT_DEC, poz.CPR_LON_DEC);
#endif
				///
				// ...	�������� �� ����� ���/����� (�����/���) - ��������� ��������� ��� ����������
				if ((bfrm.previous_value_F[0] == 1) && (bfrm.previous_value_F[1] != poz.bit54__F)) {
					// ...	��������� ����������� �������� ��������� (global)
					bfrm.previous_value_F[2] = 1;
					cprg.F[0] = bfrm.previous_value_F[1];
					cprg.F[1] = poz.bit54__F;
#ifdef visualization_of_intermediate_calculations_when_debugging
					printf("# Global: %d\t\t F[0]: %d\t\t F[1]: %d\n", bfrm.previous_value_F[2], cprg.F[0], cprg.F[1]);
#endif
					// ...  �������������� ���������� ���������
					coding_DEC_format_glob(prev.CPR_LAT_DEC, prev.CPR_LON_DEC, poz.CPR_LAT_DEC, poz.CPR_LON_DEC, poz.bit54__F);
					///
					dfex.event_log[check] = 1;
					//if (pos == 0) pos = 1;
					//else 	pos--;
				}
				else if ((bfrm.previous_value_F[0] == 1)  && (bfrm.previous_value_F[2] == 1) && (bfrm.previous_value_F[1] == poz.bit54__F)) {
					// ...	��������� ���������� �������� ��������� (local)
					bfrm.previous_value_F[2] = 0;
#ifdef visualization_of_intermediate_calculations_when_debugging
					printf("# Local: %d\t\t F: %d\n", bfrm.previous_value_F[2], poz.bit54__F);
#endif
					/// ...  �������������� ��������� ���������
					coding_DEC_format_local(poz.CPR_LAT_DEC, poz.CPR_LON_DEC, poz.bit54__F, cprg.LAT, cprg.LON);
					// ...	�������� �� ������������� �������� ���������
					if ((cprl.LAT == cprg.LAT) || (cprl.LAT == cprl.LAT_ref)) dfex.event_log[check] = 3;
					if ((cprl.LON == cprg.LON) || (cprl.LON == cprl.LON_ref)) dfex.event_log[check] = 4;
					if ((dfex.event_log[check] == 3) && (dfex.event_log[check] == 4)) dfex.event_log[check] = 5;
					// ...	���������� ������� ��������
					cprl.LAT_ref = cprl.LAT;
					cprl.LON_ref = cprl.LON;
					//pos++;
				}
				else if ((bfrm.previous_value_F[0] == 1)  && (bfrm.previous_value_F[1] == poz.bit54__F)) {
#ifdef visualization_of_intermediate_calculations_when_debugging
					printf("# Local: %d\t\t F: %d\n", bfrm.previous_value_F[2], poz.bit54__F);
#endif
					/// ...  �������������� ��������� ���������
					coding_DEC_format_local(poz.CPR_LAT_DEC, poz.CPR_LON_DEC, poz.bit54__F, cprl.LAT_ref, cprl.LON_ref);
					// ...	�������� �� ������������� �������� ���������
					if ((cprl.LAT == cprg.LAT) || (cprl.LAT == cprl.LAT_ref)) dfex.event_log[check] = 3;
					if ((cprl.LON == cprg.LON) || (cprl.LON == cprl.LON_ref)) dfex.event_log[check] = 4;
					if ((dfex.event_log[check] == 3) && (dfex.event_log[check] == 4)) dfex.event_log[check] = 5;
					// ...	���������� ������� ��������
					cprl.LAT_ref = cprl.LAT;
					cprl.LON_ref = cprl.LON;
					//pos++;
				}
				// ...	���������� ������� �������� ���������� ���������������
				prev.ALT_m = poz.ALT_m;
				prev.CPR_LAT_DEC = poz.CPR_LAT_DEC;
				prev.CPR_LON_DEC = poz.CPR_LON_DEC;
				bfrm.previous_value_F[0] = 1;
				bfrm.previous_value_F[1] = poz.bit54__F;
				///
				dfex.index[1]++;
				sdat.event_pos[check] = pos;
			}
			else if (bfrm.TC_DEC == 19) {
				dfex.event_log[check] = 2;
				// ...	frame '���������� ��������� �������'
				for (jscfr = 0; jscfr < 3; jscfr++) {
					spd.ST[jscfr] = qbit1.frame[37 + jscfr];
					spd.NAC[jscfr] = qbit1.frame[42 + jscfr];
					if (jscfr < 2) spd.RESV_B[jscfr] = qbit1.frame[78 + jscfr];
				}
				for (jscfr = 0; jscfr < 10; jscfr++) {
					spd.V_ew[jscfr] = qbit1.frame[46 + jscfr];
					spd.V_ns[jscfr] = qbit1.frame[57 + jscfr];
					if (jscfr < 9) spd.Vr[jscfr] = qbit1.frame[69 + jscfr];
					if (jscfr < 7) spd.Dif[jscfr] = qbit1.frame[81 + jscfr];
				}
				// ...	���������� ��� ��������� 'East-West Velocity'
				spd.V_ew_DEC = convertion_to_decimal(10, 10);
				// ...	���������� ��� ��������� 'North-South Velocity'
				spd.V_ns_DEC = convertion_to_decimal(11, 10);
				// ...	���������� ��� ��������� 'Vertical Velocity'
				spd.Vr_DEC = convertion_to_decimal(12, 9);
				// ...	���������� ��� ��������� 'Dif'
				spd.Dif_DEC = convertion_to_decimal(13, 7);
				// ...	���������� ��� ��������� 'ST'
				spd.ST_DEC = convertion_to_decimal(14, 3);
				// ...	���������� ��� ��������� 'NAC'
				spd.NAC_DEC = convertion_to_decimal(15, 3);
				// ...	���������� ��� ��������� 'RESV_B'
				spd.RESV_B_DEC = convertion_to_decimal(16, 2);
				///
				if (spd.bit46__S_ew == 1)
						pspd.Vwe_kt = -(spd.V_ew_DEC - 1);
				else	pspd.Vwe_kt = spd.V_ew_DEC - 1;
				pspd.Vwe_m_s = pspd.Vwe_kt * KT;
				if (spd.bit57__S_ns == 1)
						pspd.Vsn_kt = -(spd.V_ns_DEC - 1);
				else	pspd.Vsn_kt = spd.V_ns_DEC - 1;
				pspd.Vsn_m_s = pspd.Vsn_kt * KT;
				//	...	�������� � ��������� XZ
				pspd.Vgr_kt = sqrt(pow(pspd.Vwe_kt, 2) + pow(pspd.Vsn_kt, 2));
				// ...	������� �������� � �/�
				pspd.Vgr_m_s = pspd.Vgr_kt * KT;
				// ...	����������������
				if (spd.bit69__S_vr == 0)
						pspd.climb_fpm = -(spd.Vr_DEC -1) * 64;
				else	pspd.climb_fpm = (spd.Vr_DEC -1) * 64;
				pspd.climb_m_s = pspd.climb_fpm / FPM;
				// ...	��������� �������� � �/�
				pspd.Vair_m_s = sqrt(pow(pspd.Vwe_m_s, 2) + pow(pspd.Vsn_m_s, 2) + pow(pspd.climb_m_s, 2));
				// ...	������� ���� � ����. (���)
				pspd.h_deg[0] = atan2(pspd.Vwe_kt, pspd.Vsn_kt) * 180 / PI;
				if (pspd.h_deg[0] < 0)
						pspd.h_deg[1] = 360 + pspd.h_deg[0];
				else 	pspd.h_deg[1] = pspd.h_deg[0];
				pspd.h_rad = pspd.h_deg[1] * RAD;
#ifdef visualization_of_intermediate_calculations_when_debugging
				printf("# Vwe: %d kt\t\t Vsn: %d kt\t\t Vgr: %f kt\n", pspd.Vwe_kt, pspd.Vsn_kt, pspd.Vgr_kt);
				printf("# Vwe: %f m/s\t Vsn: %f m/s\t Vgr: %f m/s\n", pspd.Vwe_m_s, pspd.Vsn_m_s, pspd.Vgr_m_s);
				printf("# Vr:  %d fpm\t\t Climb: %f m/s\n", pspd.climb_fpm, pspd.climb_m_s);
				printf("# Vair: %f m/s\t h: %f deg\t = %f rad\n", pspd.Vair_m_s, pspd.h_deg[1], pspd.h_rad);
#endif
				///
				// ...	���������� ���������� � ��������� ������ ��� ����������������
				dfex.spd_sn_m_s[0][dfex.index[2]] = pspd.Vsn_m_s;
				dfex.spd_we_m_s[0][dfex.index[2]] = pspd.Vwe_m_s;
				dfex.climb_m_s[0][dfex.index[2]] = pspd.climb_m_s;
				dfex.angle_deg[0][dfex.index[2]] = pspd.h_deg[1];
				dfex.iframe[1][dfex.index[2]] = session;
				///
				dfex.index[2]++;
			}
		}
	}
	///
	return dfex.event_log[check];
}
///
/// ...  �������������� ��������� (�� CPR-������� � ����������; ���������� ����������)
int coding_DEC_format_glob(int CPR_LAT_DEC1, int CPR_LON_DEC1, int CPR_LAT_DEC2, int CPR_LON_DEC2, int bit54) {
	///
	cprg.CPR_LAT_EVEN = CPR_LAT_DEC1 / q17;
	cprg.CPR_LON_EVEN = CPR_LON_DEC1 / q17;
	cprg.CPR_LAT_ODD = CPR_LAT_DEC2 / q17;
	cprg.CPR_LON_ODD = CPR_LON_DEC2 / q17;
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# CPR_LAT_EVEN: %f\t CPR_LON_EVEN: %f\n# CPR_LAT_ODD:  %f\t CPR_LAT_ODD:  %f\n", cprg.CPR_LAT_EVEN, cprg.CPR_LON_EVEN, cprg.CPR_LAT_ODD, cprg.CPR_LON_ODD);
#endif
	///
	if ((cprg.F[0] == 0) && (cprg.F[1] == 1))
		cprg.J = (int)((((59 * CPR_LAT_DEC1) - (60 * CPR_LAT_DEC2)) / q17) + 0.5);
	else if ((cprg.F[0] == 1) && (cprg.F[1] == 0))
		cprg.J = (int)((((59 * CPR_LAT_DEC2) - (60 * CPR_LAT_DEC1)) / q17) + 0.5);
	///
	int je = (int)(cprg.J / (60 - cprg.F[0]));
	cprg.mod_even[0] = cprg.J - ((60 - cprg.F[0]) * je);
	int jo = (int)(cprg.J / (60 - cprg.F[1]));
	cprg.mod_odd[0] = cprg.J - ((60 - cprg.F[1]) * jo);
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# J: %d\t mod_even: %d\t mod_odd: %d\t F[0]: %d\t F[1]: %d\n", cprg.J, cprg.mod_even[0], cprg.mod_odd[0], cprg.F[0], cprg.F[1]);
#endif
	if (cprg.F[0] == 0)
			cprg.LAT_EVEN = (cprg.mod_even[0] + cprg.CPR_LAT_EVEN) * EVEN;
	else	cprg.LAT_EVEN = (cprg.mod_even[0] + cprg.CPR_LAT_EVEN) * ODD;
	if (cprg.LAT_EVEN >= 270) cprg.LAT_EVEN = cprg.LAT_EVEN - 360;
	///
	if (cprg.F[1] == 0)
			cprg.LAT_ODD = (cprg.mod_odd[0] + cprg.CPR_LAT_ODD) * EVEN;
	else	cprg.LAT_ODD = (cprg.mod_odd[0] + cprg.CPR_LAT_ODD) * ODD;
	if (cprg.LAT_ODD >= 270) cprg.LAT_ODD = cprg.LAT_ODD - 360;
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# LAT_EVEN: %f\t LAT_ODD: %f\n", cprg.LAT_EVEN, cprg.LAT_ODD);
#endif
	if (cprg.J % 2)
			cprg.LAT = cprg.LAT_ODD;
	else	cprg.LAT = cprg.LAT_EVEN;
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# LAT: %f\n", cprg.LAT);
#endif
	///
	cprg.NL[0] = calc_NL_glb(cprg.LAT_EVEN);
	cprg.NL[1] = calc_NL_glb(cprg.LAT_ODD);
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# NL[0]: %d\t NL[1]: %d\n", cprg.NL[0], cprg.NL[1]);
#endif
	///
	if (cprg.NL[0] > 1)
			cprg.ni[0] = cprg.NL[0] - cprg.F[0];
	else 	cprg.ni[0] = 1;
	if (cprg.NL[1] > 1)
			cprg.ni[1] = cprg.NL[1] - cprg.F[1];
	else 	cprg.ni[1] = 1;
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# ni[0]: %d\t ni[1]: %d\n", cprg.ni[0], cprg.ni[1]);
#endif
	///
	cprg.m[0] = calc_m_glb(CPR_LON_DEC1, CPR_LON_DEC2, true);
	cprg.m[1] = calc_m_glb(CPR_LON_DEC1, CPR_LON_DEC2, false);
	///
	cprg.dLON[0] = (double)360 / cprg.ni[0];
	cprg.dLON[1] = (double)360 / cprg.ni[1];
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# dLON[0]: %f\t dLON[1]: %f\n", cprg.dLON[0], cprg.dLON[1]);
#endif
	///
	int mod_e = (int)(cprg.m[0] / cprg.ni[0]);
	cprg.mod_even[1] = cprg.m[0] - (cprg.ni[1] * mod_e);
	int mod_o = (int)(cprg.m[0] / cprg.ni[1]);
	cprg.mod_odd[1] = cprg.m[0] - (cprg.ni[1] * mod_o);
	cprg.LON_EVEN = ((double)360 / cprg.ni[0]) * (cprg.mod_even[1] + cprg.CPR_LON_EVEN);
	cprg.LON_ODD = ((double)360 / cprg.ni[1]) * (cprg.mod_odd[1] + cprg.CPR_LON_ODD);
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# LON_EVEN: %f\t LON_ODD: %f\n", cprg.LON_EVEN, cprg.LON_ODD);
#endif
	if (cprg.ni[1] % 2)
			cprg.LON = cprg.LON_EVEN;
	else	cprg.LON = cprg.LON_ODD;
#ifdef visualization_of_intermediate_calculations_when_debugging
	printf("# CPR_LAT: %d\t CPR_LON: %d\t\t ALT: %d\n", poz.CPR_LAT_DEC, poz.CPR_LON_DEC, poz.ALT_DEC);
	printf("# LAT: %f\t LON: %f\t\t ALT: %f m\n", cprg.LAT, cprg.LON, poz.ALT_m);
#endif
	///
	// ...	���������� ���������� � ��������� ������ ��� ����������������
	dfex.dLAT[dfex.index[1]] = cprg.LAT;
	dfex.dLON[dfex.index[1]] = cprg.LON;
	dfex.ALT_m[dfex.index[1]] = poz.ALT_m;
	dfex.iframe[0][dfex.index[1]] = session;
	dfex.global[dfex.index[1]] = 1;
	///
	return dfex.index[1];
}
///
int calc_NL_glb(double cLAT) {
	double a, b, c;
	c = (1 - cos(PI/(2 * cprg.NZ)));
	b = pow(cos(PI * cLAT / 180), 2);
	a = acos(1 - (c / b));
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# c: %f\t b: %f\t a: %f\n", c, b, a);
#endif
	///
	return (int)(2 * PI * pow(a, -1));
}
///
int calc_m_glb(int CPR_LON1, int CPR_LON2, BOOL bm) {
	double m;
	if (bm)
	{
			if ((cprg.F[0] == 0) && (cprg.F[1] == 1))
				m = (((CPR_LON1 * (cprg.NL[0] - 1)) - (CPR_LON2 * cprg.NL[0])) / q17) + 0.5;
			else if ((cprg.F[0] == 1) && (cprg.F[1] == 0))
				m = (((CPR_LON2 * (cprg.NL[1] - 1)) - (CPR_LON1 * cprg.NL[0])) / q17) + 0.5;
	}
	else	m = ((CPR_LON1 / q17) * (cprg.NL[1] - 1)) - ((CPR_LON2 / q17) * cprg.NL[1]) + 0.5;
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# m: %f\n", m);
#endif
	///
	return (int)m;
}
/// ...  �������������� ��������� (�� CPR-������� � ����������; ��������� ����������)
int coding_DEC_format_local(int CPR_LAT_DEC1, int CPR_LON_DEC1, int bit54, double latExt, double lonExt) {
	///
	cprl.F = bit54;
	cprl.CPR_LAT = CPR_LAT_DEC1 / q17;
	cprl.CPR_LON = CPR_LON_DEC1 / q17;
	///
	cprl.dLAT = (double)360/((4 * cprl.NZ) - cprl.F);
	double a = latExt / cprl.dLAT;
	cprl.mod[0] = latExt - (cprl.dLAT * (int)a);
	///
	cprl.J = (int)a + (int)((cprl.mod[0] / cprl.dLAT) - cprl.CPR_LAT + 0.5);
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# CPR_LAT: %f\t dLAT: %f\t\t\t mod: %f\t j: %d\n", cprl.CPR_LAT, cprl.dLAT, cprl.mod[0], cprl.J);
#endif
	///
	cprl.LAT = cprl.dLAT * (cprl.J + cprl.CPR_LAT);
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# LAT: %f\n", cprl.LAT);
#endif
	///
	cprl.NL =  calc_NL_loc(latExt);
	if (cprl.NL - cprl.F == 0)
			cprl.dLON = 360;
	else	cprl.dLON = (double)360/(cprl.NL - cprl.F);
	///
	double b = (double)cprl.dLON * (int)(lonExt / cprl.dLON);
	cprl.mod[1] = lonExt - b;
	cprl.m = (int)(lonExt / cprl.dLON) + (int)((cprl.mod[1] / cprl.dLON) - cprl.CPR_LON +  0.5);
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# CPR_LON: %f\t LON_REF: %f\n", cprl.CPR_LON, lonExt);
	printf("# NL: %d\t\t dLON: %f\t\t mod: %f\t m: %d\n", cprl.NL, cprl.dLON, cprl.mod[1], cprl.m);
#endif
	///
	cprl.LON = cprl.dLON * (cprl.m + cprl.CPR_LON);
#ifdef visualization_of_intermediate_calculations_when_debugging
	printf("# CPR_LAT: %d\t CPR_LON: %d\t\t ALT: %d\n", poz.CPR_LAT_DEC, poz.CPR_LON_DEC, poz.ALT_DEC);
	printf("# LAT: %f\t LON: %f\t\t ALT: %f m\n", cprl.LAT, cprl.LON, poz.ALT_m);
#endif
	///
	dfex.dLAT[dfex.index[1]] = cprl.LAT;
	dfex.dLON[dfex.index[1]] = cprl.LON;
	dfex.ALT_m[dfex.index[1]] = poz.ALT_m;
	dfex.iframe[0][dfex.index[1]] = session;
	dfex.global[dfex.index[1]] = 0;
	///
	return dfex.index[1];
}
///
int calc_NL_loc(double RLat) {
	double a, b, c;
	c = (1 - cos(PI/(2 * cprl.NZ)));
	b = pow(cos(PI * RLat / 180), 2);
	a = acos(1 - (c / b));
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("# c: %f\t b: %f\t a: %f\n", c, b, a);
#endif
	///
	return (int)(2 * PI * pow(a, -1));
}
/// ...  �������� �������������� ��������� (�� ���������� ����� � CPR-������)
int encoding_CPR_format(BOOL bGlob, int check, int bit54, double latExt, double lonExt) {

	return 0;
}
///# ������ �������
/// ...	���������� ��������� (frame)
int message_handler(int check) {
	int jfr;
#ifdef visualization_of_intermediate_calculations_when_debugging
	printf("\nframe: %d\t", check);
#endif
	dfex.index[0] = session;
	// ...	���������� ����������� ����� CRC24 �������� frame
	CRC24_dec = calculating_frame_parity(bits_data, bAP, bCRC_clone, check);
#ifndef visualization_of_intermediate_calculations_when_debugging
	puts("\n# ����������� ����� (CRC24) �������� frame � ���������� ����:");
	printf("# dec_CRC24: %d\n", CRC24_dec);
#endif
	///
#ifdef visualization_of_intermediate_calculations_when_debugging
	puts("\n# ������� ������� ������������������ (frame):");
	jd = jd1 = 0;
#endif
	for (jfr = 0; jfr < bits_data + bits_CRC; jfr++) {
		qbit1.frame[jfr] = qbit1.bitsMx[jfr];
#ifdef visualization_of_intermediate_calculations_when_debugging
		jd++;
		if (jd == 4) {
			jd1++;
			if ((jd1 == 8) || (jd1 == 22))
					printf("%d\n", qbit1.frame[jfr]);
			else	printf("%d ", qbit1.frame[jfr]);
			jd = 0;
		}
		else 	printf("%d", qbit1.frame[jfr]);
	}
	puts("\n");
#endif
	///
	decomp_frames_into_components(1, check);
	if (dfex.event_log[check] == 1) {
		pos -= 1;
	}
	sdat.event_pos[check] = pos;
	///
#ifdef visualization_of_intermediate_calculations_when_debugging
	printf("# ������ ������� (event_log: %d;  position: %d)\n", dfex.event_log[check], sdat.event_pos[check]);
#endif
	free(frame);
	///
	return 0;
}
///
int convert_to_DSC(int ibeg, int numb) {
	///
	int jw;
	puts("\n");
	puts("\t\t\t\t\t\t\t       ������� 3");
	puts("-------------------------------------------------------------------------");
	puts("n/n\t      X\t\t      Z\t\t      Y\t\tALT, km");
	puts("-------------------------------------------------------------------------");
	for (jw = ibeg; jw < numb; jw++) {
		sdat.rLAT[jw] = radians(dfex.dLAT[jw]);
		sdat.rLON[jw] = radians(dfex.dLON[jw]);
		///
		sdat.cosB[jw] = cos(sdat.rLAT[jw]);
		sdat.sinB[jw] = sin(sdat.rLAT[jw]);
		sdat.cosL[jw] = cos(sdat.rLON[jw]);
		sdat.sinL[jw] = sin(sdat.rLON[jw]);
		sdat.cosBsinL[jw] = cos(sdat.rLAT[jw]) * sin(sdat.rLON[jw]);
		sdat.cosBcosL[jw] = cos(sdat.rLAT[jw]) * cos(sdat.rLON[jw]);
		sdat.sinBsinL[jw] = sin(sdat.rLAT[jw]) * sin(sdat.rLON[jw]);
		sdat.sinBcosL[jw] = sin(sdat.rLAT[jw]) * cos(sdat.rLON[jw]);
		sdat.N[jw] = R / (sqrt(1 - e2 * pow(sdat.sinB[jw], 2)));
#ifndef visualization_of_intermediate_calculations_when_debugging
		if (jw == 0) puts("������� ������ ������ ����� �������������� � �������:");
		printf("Bi= %f\t Li= %f\t Hi= %f �m\t Ni= %f ��\n", sdat.rLAT[jw], sdat.rLON[jw], dfex.ALT_m[jw] / 1000, sdat.N[jw]);
		///
		printf("cosB= %f\t sinB: %f\t cosL= %f\t sinL %f\n", sdat.cosB[jw], sdat.sinB[jw], sdat.cosL[jw], sdat.sinL[jw]);
		printf("cosBsinL= %f\t cosBcosL= %f\t sinBsinL= %f\t sinBcosL= %f\n", sdat.cosBsinL[jw], sdat.cosBcosL[jw], sdat.sinBsinL[jw], sdat.sinBcosL[jw]);
#endif
		// ...	�������������� �������������� ��������� �������
		//		� ���������� ���������� ������������� ������� ��������� DSC
		sdat.X[jw] = (sdat.N[jw] + (dfex.ALT_m[jw] / 1000)) * sdat.cosBsinL[jw];
		sdat.Z[jw] = (sdat.N[jw] + (dfex.ALT_m[jw] / 1000)) * sdat.cosBcosL[jw];
		sdat.Y[jw] = (sdat.N[jw] + (dfex.ALT_m[jw] / 1000)) * sdat.sinB[jw] - e2 * sdat.N[jw] * sdat.sinB[jw];
#ifdef visualization_of_intermediate_calculations_when_debugging
		printf("%d\t %f\t %f\t %f\t", jw, sdat.X[jw], sdat.Z[jw], sdat.Y[jw]);
		printf("%f\n", dfex.ALT_m[jw] / 1000);
#endif
	}
	return ibeg;
}
///
// ...
double radians(double degress) {
	return degress * PI / 180;
}
///
// ...	��������� frames
int frame_simulator(int check_frames) {
	int check;
	pos = 0;
	///
	for (check = 0; check < check_frames; check++)
	{
		session = check;
		switch (check) {
		case 0:
			frame = malloc(sizeof(char) * strlen(Mx0) + 1);
			strcpy(frame, Mx0);
			count = strlen(Mx0);
			break;
		case 1:
			frame = malloc(sizeof(char) * strlen(Mx1) + 1);
			strcpy(frame, Mx1);
			count = strlen(Mx1);
			break;
		case 2:
			frame = malloc(sizeof(char) * strlen(Mx2) + 1);
			strcpy(frame, Mx2);
			count = strlen(Mx2);
			break;
		case 3:
			frame = malloc(sizeof(char) * strlen(Mx3) + 1);
			strcpy(frame, Mx3);
			count = strlen(Mx3);
			break;
		case 4:
			frame = malloc(sizeof(char) * strlen(Mx4) + 1);
			strcpy(frame, Mx4);
			count = strlen(Mx4);
			break;
		case 5:
			frame = malloc(sizeof(char) * strlen(Mx5) + 1);
			strcpy(frame, Mx5);
			count = strlen(Mx5);
			break;
		case 6:
			frame = malloc(sizeof(char) * strlen(Mx6) + 1);
			strcpy(frame, Mx6);
			count = strlen(Mx6);
			break;
		case 7:
			frame = malloc(sizeof(char) * strlen(Mx7) + 1);
			strcpy(frame, Mx7);
			count = strlen(Mx7);
			break;
		case 8:
			frame = malloc(sizeof(char) * strlen(Mx8) + 1);
			strcpy(frame, Mx8);
			count = strlen(Mx8);
			break;
		case 9:
			frame = malloc(sizeof(char) * strlen(Mx9) + 1);
			strcpy(frame, Mx9);
			count = strlen(Mx9);
			break;
		case 10:
			frame = malloc(sizeof(char) * strlen(Mx10) + 1);
			strcpy(frame, Mx10);
			count = strlen(Mx10);
			break;
		case 11:
			frame = malloc(sizeof(char) * strlen(Mx11) + 1);
			strcpy(frame, Mx11);
			count = strlen(Mx11);
			break;
		case 12:
			frame = malloc(sizeof(char) * strlen(Mx12) + 1);
			strcpy(frame, Mx12);
			count = strlen(Mx12);
			break;
		case 13:
			frame = malloc(sizeof(char) * strlen(Mx13) + 1);
			strcpy(frame, Mx13);
			count = strlen(Mx13);
			break;
		case 14:
			frame = malloc(sizeof(char) * strlen(Mx14) + 1);
			strcpy(frame, Mx14);
			count = strlen(Mx14);
			break;
		case 15:
			frame = malloc(sizeof(char) * strlen(Mx15) + 1);
			strcpy(frame, Mx15);
			count = strlen(Mx15);
			break;
		case 16:
			frame = malloc(sizeof(char) * strlen(Mx16) + 1);
			strcpy(frame, Mx16);
			count = strlen(Mx16);
			break;
		default:
			check = session;
		}
		///
		// ...	��������� ��������� �������� ��������� (frame)
		message_handler(check);
	}
	///
#ifdef visualization_of_intermediate_calculations_when_debugging
	// ...	������������ ���������������� "�����" ������ ��� �����������������
	//		(������������� �������)
	puts("\n");
	puts("\t\t\t\t\t\t\t       ������� 1");
	puts("-------------------------------------------------------------------------");
	puts("n/n\t    LON\t\t    LAT\t\t    ALT, m\tframe\tglob");
	puts("-------------------------------------------------------------------------");
	for (jShow = 1; jShow < dfex.index[1]; jShow++) {
		printf("%d\t %f\t %f\t %f\t  %d\t  %d\n", jShow, dfex.dLON[jShow], dfex.dLAT[jShow], dfex.ALT_m[jShow], dfex.iframe[0][jShow], dfex.global[jShow]);
	}
	puts("-------------------------------------------------------------------------");
	puts("\t\t\t\t\t\t\t       ������� 2");
	puts("-------------------------------------------------------------------------");
	puts("n/n\t  Vwe, m/s\t  Vsn, m/s\t Climb, m/s\tframe");
	puts("-------------------------------------------------------------------------");
	for (jShow = 0; jShow < dfex.index[2]; jShow++) {
		printf("%d\t %f\t %f\t %f\t  %d\n", jShow, dfex.spd_we_m_s[0][jShow], dfex.spd_sn_m_s[0][jShow], dfex.climb_m_s[0][jShow], dfex.iframe[1][jShow]);
	}
	puts("-------------------------------------------------------------------------");
#else
	puts("-------------------------------------------------------------------------");
#endif
	///
	return check;
}
///
// ...	���������� ������������� �������� '���������� ����������'
int calculating_intermediate_values_speeds(int modeS) {
	int jn, jms, jsp = 0;
	int dpos[4], jb = 0;
	BOOL bp0[2];
	///
	// ...	������� ����������� ������ frames � ������� �������,
	//		���������� '����������' ���������
	for (jn = 0; jn < positions; jn++) {
		if (dfex.event_log[jn] == 2) {
			sval.arrayS[jsp] = jn;
			if (modeS == 0) {
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("%d ", arrayS[jsp]);
#endif
			}
			jsp++;
		}
	}
	if ( modeS == 0) {
#ifndef visualization_of_intermediate_calculations_when_debugging
		puts("\n- positions (local points number):");
#endif
	}
#ifndef visualization_of_intermediate_calculations_when_debugging
	if ( modeS == 0) {
		puts("\n- positions:");
		for (jn = 0; jn < positions; jn++) {
			printf("%d ", sdat.event_pos[jn]);
		}
		puts("\n- event log:");
		for (jn = 0; jn < positions; jn++) {
			printf("%d ", dfex.event_log[jn]);
		}
		puts("\n- frames 'speed':");
		for (jn = 0; jn < positions; jn++) {
			printf("%d ", dfex.iframe[1][jn]);
		}
		puts("\n- frames 'local':");
		for (jn = 0; jn < positions; jn++) {
			printf("%d ", dfex.iframe[0][jn]);
		}
		puts("\n");
	}
#endif
	///
	jsp = jms = jb = 0;
	bp0[0] = bp0[1] = false;
	///
	if (modeS == 0) {
		///
		for (jn = 0; jn < positions; jn++) {
			///
			if (jn < sval.arrayS[jms]) {
				if ((sdat.Z[dfex.iframe[0][jn]] > 0) && (!bp0[0])) {
					sval.crd0 = sdat.Z[dfex.iframe[0][jn]];
					bp0[0] = true;
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("\t\t\t\t %f\n", sval.crd0);
#endif
				}
			}
			///
			if ((jn > sval.arrayS[jms]) && (jn < sval.arrayS[jms + 1])) {
				sval.sp1 = dfex.spd_we_m_s[0][jsp];
				sval.sp2 = dfex.spd_we_m_s[0][jsp + 1];
				// ...	���������� ����� 'local' � ��������� (����� ����� ���������� ��������� 'speed')
				dpos[0] = sval.arrayS[jms + 1] - 1 - sval.arrayS[jms];
				if (dpos[0] >= 3) {
					dpos[1] = sval.arrayS[jms] + (int)(dpos[0] / 2);
					dpos[3] = dpos[1] + 1;
				}
				else	dpos[1] = sval.arrayS[jms] + 2;
				///
				dpos[2] = index_detection(dpos[1]);
				sval.index[0] = index_detection(jn);
				sval.index[1] = index_detection(dpos[1]);
				sval.crd1 = sdat.Z[sval.index[0]];
				//
				if (dpos[0] >= 3) {
					if (dfex.event_log[sval.index[1]] == 1)
						sval.crd2 = sdat.Z[sval.index[1]];
					else {
						sval.index[1] = index_detection(dpos[3]);
						sval.crd2 = sdat.Z[sval.index[1]];
						dpos[1] = dpos[3];
					}
				}
				else	sval.crd2 = sdat.Z[sval.index[1]];
				///
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("%d -(%d)- %d\t [%d; %d]\n", jn, dpos[1], sval.arrayS[jms + 1] - 1, sval.index[0], sval.index[1]);
#endif
				// ...	�������� �� ���������� ����� � ���������
				//		(���� ������ 3, �� �������� ��������� �� ������ ������� ���������� ���������)
				if (dpos[0] >= 3)
						sval.index[2] = index_detection(sval.arrayS[jms + 1] - 1);
				else	sval.index[2] = index_detection(sval.arrayS[jms + 1] + 1);
				sval.crd3 = sdat.Z[sval.index[2]];
				///
				sval.crd21 = sval.crd2 - sval.crd1;
				if (sval.crd21 < 0) sval.crd21 = -sval.crd21;
				sval.crd31 = sval.crd3 - sval.crd1;
				if (sval.crd31 < 0) sval.crd31 = -sval.crd31;
				if (sval.crd31 != 0)
						sval.sp21 = (sval.crd21/sval.crd31) * (sval.sp2 - sval.sp1) + sval.sp1;
				else	sval.sp21 = 0;
				///
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("\t\t %f\t %f\n\t\t %f\t %f\t %d\n", sval.sp1, sval.crd1, sval.sp21, sdat.Z[sval.index[1]], dpos[1]);
				printf("%d\t %d\t %f\t %f\n", dpos[1], sval.arrayS[jms], sval.sp2, sval.crd3);
#endif
				///
				// ...	���������� ����������� � ��������� 'primary_data_for_extrapolation'
				if ((bp0[0]) && (!bp0[1])) {
					dprm.loc[0][jb] = sval.crd0;
					bp0[1] = true;
				}
				dprm.loc[0][jb + 1] = sval.crd1;
				dprm.spd[0][jb + 1] = sval.sp1;
				dprm.loc[0][jb + 2] = sdat.Z[sval.index[1]];
				dprm.spd[0][jb + 2] = sval.sp21;
				dprm.loc[0][jb + 3] = sval.crd3;
				dprm.spd[0][jb + 3] = sval.sp2;
				///
				jms += 1;
				jsp += 1;
				jb += 2;
			}
		}
	}
	///
	else if (modeS == 1) {
		jsp = jms = jb = 0;
		bp0[0] = bp0[1] = false;
		///
		for (jn = 0; jn < positions; jn++) {
			if (jn < sval.arrayS[jms]) {
				if ((sdat.X[dfex.iframe[0][jn]] > 0) && (!bp0[0])) {
					sval.crd0 = sdat.X[dfex.iframe[0][jn]];
					bp0[0] = true;
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("\t\t\t\t %f\n", sval.crd0);
#endif
				}
			}
			if ((jn > sval.arrayS[jms]) && (jn < sval.arrayS[jms + 1])) {
				sval.sp1 = dfex.spd_sn_m_s[0][jsp];
				sval.sp2 = dfex.spd_sn_m_s[0][jsp + 1];
				// ...	���������� ����� 'local' � ��������� (����� ����� ���������� ��������� 'speed')
				dpos[0] = sval.arrayS[jms + 1] - 1 - sval.arrayS[jms];
				if (dpos[0] >= 3) {
					dpos[1] = sval.arrayS[jms] + (int)(dpos[0] / 2);
					dpos[3] = dpos[1] + 1;
				}
				else	dpos[1] = sval.arrayS[jms] + 2;
				///
				dpos[2] = index_detection(dpos[1]);
				sval.index[0] = index_detection(jn);
				sval.index[1] = index_detection(dpos[1]);
				sval.crd1 = sdat.X[sval.index[0]];
				//
				if (dpos[0] >= 3) {
					if (dfex.event_log[sval.index[1]] == 1)
						sval.crd2 = sdat.X[sval.index[1]];
					else {
						sval.index[1] = index_detection(dpos[3]);
						sval.crd2 = sdat.X[sval.index[1]];
						dpos[1] = dpos[3];
					}
				}
				else	sval.crd2 = sdat.X[sval.index[1]];
				///
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("%d -(%d)- %d\t [%d; %d]\n", jn, dpos[1], sval.arrayS[jms + 1] - 1, sval.index[0], sval.index[1]);
#endif
				// ...	�������� �� ���������� ����� � ���������
				//		(���� ������ 3, �� �������� ��������� �� ������ ������� ���������� ���������)
				if (dpos[0] >= 3) {
					sval.index[2] = index_detection(sval.arrayS[jms + 1] - 1);
					sval.crd3 = sdat.X[sval.index[2]];
				}
				else {
					sval.index[3] = index_detection(sval.arrayS[jms + 1] + 1);
					sval.crd3 = sdat.X[sval.index[3]];
				}
				///
				sval.crd21 = sval.crd2 - sval.crd1;
				if (sval.crd21 < 0) sval.crd21 = -sval.crd21;
				sval.crd31 = sval.crd3 - sval.crd1;
				if (sval.crd31 < 0) sval.crd31 = -sval.crd31;
				if (sval.crd31 != 0)
						sval.sp21 = (sval.crd21/sval.crd31) * (sval.sp2 - sval.sp1) + sval.sp1;
				else	sval.sp21 = 0;
				///
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("\t\t %f\t %f\n\t\t %f\t %f\n", sval.sp1, sval.crd1, sval.sp21, sdat.X[sval.index[1]]);
				printf("%d\t %d\t %f\t %f\n", dpos[1], sval.arrayS[jms], sval.sp2, sval.crd3);
#endif
				///
				// ...	���������� ����������� � ��������� 'primary_data_for_extrapolation'
				if ((bp0[0]) && (!bp0[1])) {
					dprm.loc[1][jb] = sval.crd0;
					bp0[1] = true;
				}
				dprm.loc[1][jb + 1] = sval.crd1;
				dprm.spd[1][jb + 1] = sval.sp1;
				dprm.loc[1][jb + 2] = sdat.X[sval.index[1]];
				dprm.spd[1][jb + 2] = sval.sp21;
				dprm.loc[1][jb + 3] = sval.crd3;
				dprm.spd[1][jb + 3] = sval.sp2;
				///
				jms += 1;
				jsp += 1;
				jb += 2;
			}
		}
	}
	///
	else if (modeS == 2) {
		jsp = jms = jb = 0;
		bp0[0] = bp0[1] = false;
		///
		for (jn = 0; jn < positions; jn++) {
			if (jn < sval.arrayS[jms]) {
				if ((sdat.Y[dfex.iframe[0][jn]] > 0) && (!bp0[0])) {
					sval.crd0 = sdat.Y[dfex.iframe[0][jn]];
					bp0[0] = true;
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("\t\t\t\t %f\n", sval.crd0);
#endif
				}
			}
			if ((jn > sval.arrayS[jms]) && (jn < sval.arrayS[jms + 1])) {
				sval.sp1 = dfex.climb_m_s[0][jsp];
				sval.sp2 = dfex.climb_m_s[0][jsp + 1];
				// ...	���������� ����� 'local' � ��������� (����� ����� ���������� ��������� 'speed')
				dpos[0] = sval.arrayS[jms + 1] - 1 - sval.arrayS[jms];
				if (dpos[0] >= 3) {
					dpos[1] = sval.arrayS[jms] + (int)(dpos[0] / 2);
					dpos[3] = dpos[1] + 1;
				}
				else	dpos[1] = sval.arrayS[jms] + 2;
				///
				dpos[2] = index_detection(dpos[1]);
				sval.index[0] = index_detection(jn);
				sval.index[1] = index_detection(dpos[1]);
				sval.crd1 = sdat.Y[sval.index[0]];
				//
				if (dpos[0] >= 3) {
					if (dfex.event_log[sval.index[1]] == 1)
						sval.crd2 = sdat.Y[sval.index[1]];
					else {
						sval.index[1] = index_detection(dpos[3]);
						sval.crd2 = sdat.Y[sval.index[1]];
						dpos[1] = dpos[3];
					}
				}
				else	sval.crd2 = sdat.Y[sval.index[1]];
				///
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("%d -(%d)- %d\t [%d; %d]\n", jn, dpos[1], sval.arrayS[jms + 1] - 1, sval.index[0], sval.index[1]);
#endif
				// ...	�������� �� ���������� ����� � ���������
				//		(���� ������ 3, �� �������� ��������� �� ������ ������� ���������� ���������)
				if (dpos[0] >= 3) {
					sval.index[2] = index_detection(sval.arrayS[jms + 1] - 1);
					sval.crd3 = sdat.Y[sval.index[2]];
				}
				else {
					sval.index[3] = index_detection(sval.arrayS[jms + 1] + 1);
					sval.crd3 = sdat.Y[sval.index[3]];
				}
				///
				sval.crd21 = sval.crd2 - sval.crd1;
				if (sval.crd21 < 0) sval.crd21 = -sval.crd21;
				sval.crd31 = sval.crd3 - sval.crd1;
				if (sval.crd31 < 0) sval.crd31 = -sval.crd31;
				if (sval.crd31 != 0)
						sval.sp21 = (sval.crd21/sval.crd31) * (sval.sp2 - sval.sp1) + sval.sp1;
				else	sval.sp21 = 0;
				///
#ifndef visualization_of_intermediate_calculations_when_debugging
				printf("\t\t %f\t %f\n\t\t %f\t %f\n", sval.sp1, sval.crd1, sval.sp21, sdat.Y[sval.index[1]]);
				printf("%d\t %d\t %f\t %f\n", dpos[1], sval.arrayS[jms], sval.sp2, sval.crd3);
#endif
				///
				// ...	���������� ����������� � ��������� 'primary_data_for_extrapolation'
				if ((bp0[0]) && (!bp0[1])) {
					dprm.loc[2][jb] = sval.crd0;
					bp0[1] = true;
				}
				dprm.loc[2][jb + 1] = sval.crd1;
				dprm.spd[2][jb + 1] = sval.sp1;
				dprm.loc[2][jb + 2] = sdat.Y[sval.index[1]];
				dprm.spd[2][jb + 2] = sval.sp21;
				dprm.loc[2][jb + 3] = sval.crd3;
				dprm.spd[2][jb + 3] = sval.sp2;
				///
				jms += 1;
				jsp += 1;
				jb += 2;
			}
		}
	}
	///
	return 0;
}
///
// ...	��������� (�����������) ������� � ��c����
int index_detection(int indexP) {
	///
	int jscP, result;
	// ...
	for (jscP = 0; jscP < positions; jscP++) {
		if (dfex.iframe[0][jscP] == indexP) {
			result = jscP;
			break;
		}
	}
	return result;
}
///
// ...	���������� ����������, ������� �������� � ���������� ������� ����� ������� ����������
int distance_calculation_plus(int countD, int modeD) {
	int jsc;
	///
	for (jsc = 0; jsc < countD; jsc++) {
		if (modeD == 0) {
			// ...	���������� ����� ������� ���������� � ������������ � ������
			dprm.dist[0][jsc + 1] = 1000 * sqrt(pow(dprm.loc[0][jsc] - dprm.loc[0][jsc + 1], 2) + pow(dprm.loc[1][jsc] - dprm.loc[1][jsc + 1], 2) + pow(dprm.loc[2][jsc] - dprm.loc[2][jsc + 1], 2));
		}
		else if (modeD == 1) {
			// ...	���������� ����� ������� ���������� � ��������� XZ � ������
			dprm.dist[1][jsc + 1] = 1000 * sqrt(pow(dprm.loc[0][jsc] - dprm.loc[0][jsc + 1], 2) + pow(dprm.loc[1][jsc] - dprm.loc[1][jsc + 1], 2));
		}
	}
	///
	for (jsc = 0; jsc < countD; jsc++) {
		if (modeD == 0) {
			// ...	������� �������� � �/� (� ������������)
			dprm.Vgr[0][jsc] = sqrt(pow(dprm.spd[0][jsc], 2) + pow(dprm.spd[1][jsc], 2) + pow(dprm.spd[2][jsc], 2));
			// ...	���������� ��������� ����� ����� ������� ���������� � ��� (� ������������)
			dprm.dt[0][jsc] = dprm.dist[0][jsc] / dprm.Vgr[0][jsc];
		}
		else if (modeD == 1) {
			// ...	�������� � �/� � ��������� XZ
			dprm.Vgr[1][jsc] = sqrt(pow(dprm.spd[0][jsc], 2) + pow(dprm.spd[1][jsc], 2));
			// ...	���������� ��������� ����� ����� ������� ���������� � ��� (� ��������� XZ)
			dprm.dt[1][jsc] = dprm.dist[1][jsc] / dprm.Vgr[1][jsc];
		}
	}
	///
	// ...	��������� ����� �� ������ ���������
	dprm.t[modeD][0] = 0;
	for (jsc = 1; jsc < countD; jsc++) {
		dprm.t[modeD][jsc] = dprm.dt[modeD][jsc] + dprm.t[modeD][jsc - 1];
	}
	///
#ifdef visualization_of_intermediate_calculations_when_debugging
	for (jsc = 1; jsc < countD; jsc++) {
		printf("    %d\t   %f\t %f\t %f\t %f\n", jsc, dprm.dist[modeD][jsc], dprm.Vgr[modeD][jsc], dprm.dt[modeD][jsc], dprm.t[modeD][jsc]);
	}
#endif
	///
	return modeD;
}
///
// ...	���������� ������� �� ������������ � ���������� ������� ��������
int determinant_matrix(int check, int module, BOOL bmode) {
    _B1 = check;
    _B2 = dMat.b[1];
    _C1 = dMat.c[0];
    _C2 = dMat.c[1];
    _D1 = dMat.d[0];
    _D2 = dMat.d[1];
    if (!bmode) {
    	_B3 = dMat.b[2];
        _C3 = dMat.c[2];
        _A1 = dMat.a[0];
        _A2 = dMat.a[1];
        _A3 = dMat.a[2];
        _D3 = dMat.d[2];
    }
    else {
        _B3 = _C3 = _A1 = _A2 = _D3 = 0;
        _A3 = 1;
    }
    // ...	���������� ������������� �������
    __G1 = _B1 * _C2 * _A3;
    __G2 = _A1 * _B2 * _C3;
    __G3 = _C1 * _A2 * _B3;
    __G4 = _A1 * _C2 * _B3;
    __G5 = _B1 * _A2 * _C3;
    __G6 = _C1 * _B2 * _A3;
    dMat.det[0] = (__G1 + __G2 + __G3 - __G4 - __G5 - __G6) * pow(10, -9);
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
    printf("det[0]: %f\n", dMat.det[0]);
#endif
    //       ...
    __H1 = _D1 * _C2 * _A3;
    __H2 = _D3 * _B2 * _C3;
    __H3 = _D2 * _A2 * _B3;
    __H4 = _D3 * _C2 * _B3;
    __H5 = _D1 * _A2 * _C3;
    __H6 = _D2 * _B2 * _A3;
    dMat.det[1] = (__H1 + __H2 + __H3 - __H4 - __H5 - __H6) * pow(10, -9);
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
    printf("det[1]: %f\n", dMat.det[1]);
#endif
    //      ...
    __J1 = _B1 * _D2 * _A3;
    __J2 = _A1 * _D1 * _C3;
    __J3 = _C1 * _D3 * _B3;
    __J4 = _A1 * _D2 * _B3;
    __J5 = _B1 * _D3 * _C3;
    __J6 = _C1 * _D1 * _A3;
    dMat.det[2] = (__J1 + __J2 + __J3 - __J4 - __J5 - __J6) * pow(10, -9);
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
    printf("det[2]: %f\n", dMat.det[2]);
#endif
    //      ...
    __K1 = _B1 * _C2 * _D3;
    __K2 = _A1 * _B2 * _D2;
    __K3 = _C1 * _A2 * _D1;
    __K4 = _A1 * _C2 * _D1;
    __K5 = _B1 * _A2 * _D2;
    __K6 = _C1 * _B2 * _D3;
    dMat.det[3] = (__K1 + __K2 + __K3 - __K4 - __K5 - __K6) * pow(10, -9);
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
    printf("det[3]: %f\n", dMat.det[3]);
#endif
    if (dMat.det[0] != 0) {
    	// ...  ���������� ������������� ������ ...
        dMat.R1[module] = dMat.det[1] / dMat.det[0];
        dMat.R2[module] = dMat.det[2] / dMat.det[0];
        dMat.R3[module] = dMat.det[3] / dMat.det[0];
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
        printf("R1: %f\t R2: %f\t R3: %f\n", dMat.R1[module], dMat.R2[module], dMat.R3[module]);
#endif
   	return module;
    }
    else
    {
    	// ...  ������� ����������� ...
        dMat.R1[module] = dMat.R2[module] = dMat.R3[module] = 0;
        return 11;
   }
}
///
// ...	���������� ���������� ����� ����� ������� ���������� ��������
//		(���������� ����� ��������� � ���������� ������������� ������� ���������)
double calc_distance_between_two_points(int ip1, int ip2) {
	///
	return sqrt(pow(sdat.X[ip1] - sdat.X[ip2], 2) + pow(sdat.Y[ip1] - sdat.Y[ip2], 2) + pow(sdat.Z[ip1] - sdat.Z[ip2], 2)) * 1000;
}
///
// ...	���������� ���������� ��������� ����� ����� ������� ���������� ��������
double time_interval_calculation(int ip1, int ip2) {
	///
	if (dfex.spd_m_s[1][ip2] > 0) {
		// ...	���������� �������� �������
		//flight_time = flight_time + distance[ip1] / mSou.spd_m_s[ip2];
		return sdat.distance[ip1] / dfex.spd_m_s[1][ip2];
	}
	else	return 0;
}
///
// ...	���������� ������������� �������������
int calculation_of_approximation_coefficients(int clone, int check, int module) {
	///
	dde.st[module] = dde.sP[module] = dde.rst2[module] = dde.stP[module] = 0;
	dde.rst3[module] = dde.rst4[module] = dde.sPrt2[module] = 0;
	///
	if (module < 3) {
		dde.ti[0] = 0;
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
		printf("ti: %f\n", dde.ti[0]);
#endif
		for (int jcalc = 1; jcalc < check; jcalc++) {
			dde.ti[jcalc] = dde.ti[jcalc - 1] + dfex.interval[jcalc - 1];
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
			printf("ti: %f\n", dde.ti[jcalc]);
#endif
		}
	}
	else {
		for (int jcalc = 0; jcalc < check; jcalc++) {
			dde.ti[jcalc] = dfex.interval[jcalc];
			pk.tk[clone][jcalc] = dfex.interval[jcalc];
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
			printf("check: %d\t interval: %f\n", jcalc, dde.ti[jcalc]);
		}
		puts("- - - - - - - - -");
#else
		}
#endif
	}
	///
	for (int icalc = 0; icalc < check; icalc++) {
		if (module < 4) {
			dde.st[module] = dde.st[module] + dde.ti[icalc];
			dde.rst2[module] = dde.rst2[module] + pow(dde.ti[icalc], 2);
			dde.rst3[module] = dde.rst3[module] + pow(dde.ti[icalc], 3);
			dde.rst4[module] = dde.rst4[module] + pow(dde.ti[icalc], 4);
		}
		else {
			dde.st[module] = dde.st[module] + pk.tk[clone][icalc];
			dde.rst2[module] = dde.rst2[module] + pow(pk.tk[clone][icalc], 2);
			dde.rst3[module] = dde.rst3[module] + pow(pk.tk[clone][icalc], 3);
			dde.rst4[module] = dde.rst4[module] + pow(pk.tk[clone][icalc], 4);
		}
		///
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
		printf("clone: %d  i: %d   Xk: %f\t Zk: %f\t Yk: %f\n", clone, icalc, pk.Xk[clone][icalc], pk.Zk[clone][icalc], pk.Yk[clone][icalc]);
#endif
		///
		switch (module) {
			case 1:
				dde.sP[module] = dde.sP[module] + dfex.dLON[icalc];
				dde.stP[module] = dde.stP[module] + dfex.dLON[icalc] * dde.ti[icalc];
				dde.sPrt2[module] = dde.sPrt2[module] + dfex.dLON[icalc] * pow(dde.ti[icalc], 2);
				break;
			case 2:
				dde.sP[module] = dde.sP[module] + dfex.ALT_m[icalc];
				dde.stP[module] = dde.stP[module] + dfex.ALT_m[icalc] * dde.ti[icalc];
				dde.sPrt2[module] = dde.sPrt2[module] + dfex.ALT_m[icalc] * pow(dde.ti[icalc], 2);
				break;
			case 3:
				dde.sP[module] = dde.sP[module] + sdat.X[icalc];
				dde.stP[module] = dde.stP[module] + sdat.X[icalc] * dde.ti[icalc];
				dde.sPrt2[module] = dde.sPrt2[module] + sdat.X[icalc] * pow(dde.ti[icalc], 2);
				break;
			case 4:
			case 7:
			case 10:
			case 13:
			case 16:
			case 19:
			case 22:
			case 25:
			case 28:
			case 31:
				dde.sP[module] = dde.sP[module] + pk.Xk[clone][icalc];
				dde.stP[module] = dde.stP[module] + pk.Xk[clone][icalc] * pk.tk[clone][icalc];
				dde.sPrt2[module] = dde.sPrt2[module] + pk.Xk[clone][icalc] * pow(pk.tk[clone][icalc], 2);
				break;
			case 5:
			case 8:
			case 11:
			case 14:
			case 17:
			case 20:
			case 23:
			case 26:
			case 29:
			case 32:
				dde.sP[module] = dde.sP[module] + pk.Zk[clone][icalc];
				dde.stP[module] = dde.stP[module] + pk.Zk[clone][icalc] * pk.tk[clone][icalc];
				dde.sPrt2[module] = dde.sPrt2[module] + pk.Zk[clone][icalc] * pow(pk.tk[clone][icalc], 2);
				break;
			case 6:
			case 9:
			case 12:
			case 15:
			case 18:
			case 21:
			case 24:
			case 27:
			case 30:
			case 33:
				dde.sP[module] = dde.sP[module] + pk.Yk[clone][icalc];
				dde.stP[module] = dde.stP[module] + pk.Yk[clone][icalc] * pk.tk[clone][icalc];
				dde.sPrt2[module] = dde.sPrt2[module] + pk.Yk[clone][icalc] * pow(pk.tk[clone][icalc], 2);
				break;
		default:
				dde.sP[0] = dde.sP[0] + dfex.dLAT[icalc];
				dde.stP[0] = dde.stP[0] + dfex.dLAT[icalc] * dde.ti[icalc];
				dde.sPrt2[0] = dde.sPrt2[0] + dfex.dLAT[icalc] * pow(dde.ti[icalc], 2);
		}
	}
	///
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
	printf("st: %f\t st2: %f\t st3: %f\t st4: %f\n", dde.st[module], dde.rst2[module], dde.rst3[module], dde.rst4[module]);
	printf("sP: %f\t stP: %f\t sPt2: %f\n", dde.sP[module], dde.stP[module], dde.sPrt2[module]);
#endif
	///
	dMat.b[1] = dde.st[module];
	dMat.b[2] = dde.rst2[module];
	///
	dMat.c[0] = dMat.b[1];
	dMat.c[1] = dMat.b[2];
	dMat.c[2] = dde.rst3[module];
	///
	dMat.a[0] = dMat.b[2];
	dMat.a[1] = dMat.c[2];
	dMat.a[2] = dde.rst4[module];
	///
	dMat.d[0] = dde.sP[module];
	dMat.d[1] = dde.stP[module];
	dMat.d[2] = dde.sPrt2[module];
	return module;
}
///
// ...	������������� �������� ������ ���������� �����
//		� ������ ��������� (�����������������) ���������� ��������� ��� ������ ����������
int initialization_of_flight_path_source_data(int clone, int points, double medium_s) {
	int jst, jdt, jj, jcol, jrow, check_tsc;
	BOOL bpTSC, bfirst;
	///
	// ...	������������� ����������� �������������� ��������� ������ ��������� (��� ������ ���),
	//		������ �� ���������������� ������������� ������������
	p0.Bo[clone] = dMat.R1[0] + dMat.R2[0] * medium_s + dMat.R3[0] * pow(medium_s, 2);
	p0.Lo[clone] = dMat.R1[1] + dMat.R2[1] * medium_s + dMat.R3[1] * pow(medium_s, 2);
	p0.Ho[clone] = dMat.R1[2] + dMat.R2[2] * medium_s + dMat.R3[2] * pow(medium_s, 2);
	///
	dfex.dLAT[event] = p0.Bo[clone];
	dfex.dLON[event] = p0.Lo[clone];
	dfex.ALT_m[event] = p0.Ho[clone];
	// ...	���������� ��������� ������ ��� � ������������� ������� ���������
	convert_to_DSC(event, event + 1);
	p0.X0[clone] = sdat.X[event];
	p0.Z0[clone] = sdat.Z[event];
	p0.Y0[clone] = sdat.Y[event];
#ifndef visualization_of_intermediate_calculations_extrapolation
	printf("���������� ������ ��� (clone %d):\n", clone);
	printf("Bo: %f\t Lo: %f\t\t Ho: %f �\n", p0.Bo[clone], p0.Lo[clone], p0.Ho[clone]);
	printf("Xo= %f\t Zo= %f\t Yo= %f\n", p0.X0[clone], p0.Z0[clone], p0.Y0[clone]);
	puts("- - - - - - - - -");
#endif
	///
	double buff_dt[event + 1];
	buff_dt[0] = 0;
	jst = 1;
	for (jdt = 1; jdt < points + 1; jdt++) {
		dfex.interval[event] = sdat.distance[jdt - 1] / dfex.spd_m_s[1][jdt];
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
		printf("dt: %f\t dist: %f\t speed: %f\n", dfex.interval[event], sdat.distance[jdt - 1], dfex.spd_m_s[1][jdt]);
		puts("- - - - - - - - -");
#endif
		if (dfex.interval[event] >= p0.time_tsc) {
			buff_dt[jst] = p0.time_tsc;
			buff_dt[jst + 1] = dfex.interval[event] - p0.time_tsc;
			jst = jst + 2;
		}
		else {
			buff_dt[jst] = dfex.interval[event];
			jst++;
		}
	}
	for (jst = 0; jst < points + 1; jst++) dfex.interval[jst] = 0;
	for (jst = 0; jst < points + 1; jst++)
	{
				if (jst == 0)
					dfex.interval[jst] = buff_dt[jst] = 0;
				else	dfex.interval[jst] = buff_dt[jst] + dfex.interval[jst - 1];
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
				printf("dt: %f\t sdt: %f\n", buff_dt[jst], dfex.interval[jst]);
#endif
	}
	///
	jrow = check_tsc = 0;
	bpTSC = bfirst = false;
	///
	for (jj = 0; jj < points; jj++) {
		// ...	���������� ��������� � ������������� ������� ���������
		convert_to_DSC(jj, jj + 1);
		///
		if (jj == 0) {
			// ...	���������� � ��������� ���������� �������� ��������� ��������� �����
			//		�� ���� ������������� ���������� ������� (������ �������)
			for (jcol = 0; jcol < clones; jcol++) {
				saving_coordinates_as_point_by_trajectory(jcol, jj, jrow, bpTSC);
			}
			jcol = 0;
			bfirst = true;
#ifndef visualization_of_intermediate_calculations_extrapolation
			printf("���������� �� ��������� ����� ���������� �� ������ ���: %f\n", pk.distPo[0]);
			puts("- - - - - - - - -");
#endif
		}
		else {
			// ...	�������� �� ���������� �� ��������� �����
			pk.distPk = calc_distance_between_two_points(0, jj);
			//		... � ���������� ������� � ��� ������, ���� ������� �����������
			if ((jj > 1) && (pk.distPk > pk.distPo[0]) && (check_tsc == 0)) check_tsc = jj;
			///
			saving_coordinates_as_point_by_trajectory(jcol, jj, jrow, bpTSC);
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
			printf("���������� ����� %d:\t clone: %d\n", jj + 1, clone);
			printf("Bj: %f\t Lj: %f\t Hj: %f �\n", pk.Bk[clone][jj], pk.Lk[clone][jj], pk.Hk[clone][jj]);
/////			printf("X = %f\t Z = %f\t Y = %f\n", X[jj], Z[jj], Y[jj]);
			printf("Xk= %f\t Zk= %f\t Yk= %f\n", pk.Xk[clone][jj], pk.Zk[clone][jj], pk.Yk[clone][jj]);
			printf("���������� ����� ��������� � ������� ������� ����������: %f �\t step: %d\n", pk.distPk, jj);
			puts("- - - - - - - - -");
#endif
			///
		}
#ifndef visualization_of_intermediate_calculations_extrapolation
		///
		if (jj == 0)
				printf("���������� ����� ��������� ������ � ������� ���: %f �\t step: %d\n", pk.distPo[0], jj);
		else	printf("���������� ����� ��������� � ������� ������� ����������: %f �\t step: %d\n", pk.distPk, jj);
		puts("- - - - - - - - -");
		///
		jrow++;
	}
	puts("- - - - - - - - -");
#else
		///
		jrow++;
	}
#endif
	///
#ifndef visualization_of_intermediate_calculations_extrapolation
	for (jj = 0; jj < points; jj++) {
		printf("index: %d\t Xk= %f\t Zk= %f\t Yk= %f\n", jj, pk.Xk[0][jj], pk.Zk[0][jj], pk.Yk[0][jj]);
	}
#endif
	return 0;
}
///
// ...	���������� ��������� ��� ����� �� ����������
double saving_coordinates_as_point_by_trajectory(int clone, int index, int pos, BOOL bpTSC) {
	///
	if (!bpTSC) {
		pk.Bk[clone][pos] = dfex.dLAT[index];
		pk.Lk[clone][pos] = dfex.dLON[index];
		pk.Hk[clone][pos] = dfex.ALT_m[index];
//		// ...	���������� (�������������) ��������� � ������������� ������� ���������
//		status = convert_to_radians_plus_DSC(index, index + 1);
		///
		pk.Xk[clone][pos] = sdat.X[index];
		pk.Zk[clone][pos] = sdat.Z[index];
		pk.Yk[clone][pos] = sdat.Y[index];
#ifdef visualization_of_intermediate_calculations_when_debugging
		if ((clone == 0) && (!bfirst)) {
			puts("�������� ��������� �������� ��������� ��� ������� ����������:");
			printf("���������� ����� %d:\t clone: %d\t pos: %d\n", index, clone, pos);
			printf("Bi: %f\t\t Li: %f\t\t Hi: %f �\n", pk.Bk[clone][pos],  pk.Lk[clone][pos], pk.Hk[clone][pos]);
			printf("Xi: %f\t\t Zi: %f\t Yi: %f\n", pk.Xk[clone][pos],  pk.Zk[clone][pos], pk.Yk[clone][pos]);
			puts("- - - - - - - - -");
		}
#endif
	}
	else {
		pk.Bk[clone][pos] = p0.Bo[clone];
		pk.Lk[clone][pos] = p0.Lo[clone];
		pk.Hk[clone][pos] = p0.Ho[clone];
		///
		pk.Xk[clone][pos] = p0.X0[clone];
		pk.Zk[clone][pos] = p0.Z0[clone];
		pk.Yk[clone][pos] = p0.Y0[clone];
		return 0;
	}
	///
	if (pos == 0) {
		pk.tk[clone][pos] = 0;
#ifdef visualization_of_intermediate_calculations_when_debugging
		puts("- - - - - - - - -");
		printf(": %d\t LAT1: %f\t LON1: %f\t ALT1: %f �\n", index + 1, dfex.dLAT[index], dfex.dLON[index], dfex.ALT_m[index]);
		printf(": %d\t X1: %f\t Z1: %f\t Y1: %f\n", pos + 1, pk.Xk[clone][pos], pk.Zk[clone][pos], pk.Yk[clone][pos]);
#endif
	}
	else {
		pk.tk[clone][pos] = dfex.interval[pos];
#ifndef visualization_of_intermediate_calculations
#ifdef console
		printf("Point coordinates %d:\t clone: %d\t pos: %d\n", index, clone, pos);
#else
		printf("���������� ����� %d:\t clone: %d\t pos: %d\n", index, clone, pos);
#endif
		printf("LAT: %f\t\t LON: %f\t\t ALT: %f �\n", dfex.dLAT[index], dfex.dLON[index], dfex.ALT_m[index]);
		printf("Xk: %f\t\t Zk: %f\t Yk: %f\n", pk.Xk[clone][pos], pk.Zk[clone][pos], pk.Yk[clone][pos]);
#endif
	}
	///
	if (index == 0) {
		// ...	���������� ���������� ����� ������� - ��������� � ������� ���
		//		(��� ����������� �������� �� ���������� �� ��������� �����)
		pk.distPo[clone] = sqrt(pow(pk.Xk[clone][0] - p0.X0[clone], 2) + pow(pk.Zk[clone][0] - p0.Z0[clone], 2) + pow(pk.Yk[clone][0] - p0.Y0[clone], 2)) * 1000;
	}
	return index;
}
///

int main(void) {
	///
	e2 = (2 * sj) - pow(sj, 2);
	///
	sSymb = malloc(sizeof(char) * strlen(symbol) + 1);
	strcpy(sSymb, symbol);
	parity = malloc(sizeof(char) * strlen(no_parity) + 1);
	strcpy(parity, no_parity);
	sGx = malloc(sizeof(char) * strlen(Gx) + 1);
	strcpy(sGx, Gx);
	sbGx = malloc(sizeof(char) * strlen(sbits_Gx) + 1);
	strcpy(sbGx, sbits_Gx);
	///
	sICAO = malloc(sizeof(char) * strlen(AP) + 1);
	strcpy(sICAO, AP);

#ifndef visualization_of_intermediate_calculations_when_debugging
	if (qbit.numb == 1)			printf("Gx: '%s'\t parity: '%s'\n", sGx, parity);
	else if (qbit.numb == 2)	printf("Mx: '%s'\t parity: '%s'\n", frame, parity);
	else if (qbit.numb == 3)	printf("ICAO: '%s'\t parity: '%s'\n", sICAO, parity);
	printf("symbol: '%s'\n", sSymb);
	puts("- - - - - - - - -");
#endif
	///
	// ...	��������� ��������� ������ "F"
	bfrm.previous_value_F[0] = bfrm.previous_value_F[1] = 0;
	bmode = false;
	jmode = 1;
	hpos = ishift = 0;
	///
	bits_Gx = strlen(Gx) * 4 - 3;
#ifndef visualization_of_intermediate_calculations_when_debugging
	printf("'%s'\n", sbGx);
#endif
	for (int jrw = 0; jrw < strlen(sbGx); jrw++) {
		if (sbGx[jrw] == '1')
				qbit.bitsPx[jrw] = 1;
		else 	qbit.bitsPx[jrw] = 0;
#ifndef visualization_of_intermediate_calculations_when_debugging
		printf("%d", qbit.bitsPx[jrw]);
#endif
	}
	puts("\n");
	// ...	���������� ����������� ����� CRC24 frame
	bits_data = 88;
	bAP = false;
	bCRC_clone = true;
	cprg.NZ = cprl.NZ = cNZ;
	///
	// ...	��������� frames
	frame_simulator(positions);
	///
	// ...	�������������� ��������� � DSC (������������ ������� ���������)
	convert_to_DSC(1, dfex.index[1]);
	///
#ifdef visualization_of_intermediate_calculations_when_debugging
	puts("-------------------------------------------------------------------------");
	puts("\n# ������� �������� ������������ ����������� �������������������� ��������");
	puts("���� '����������' ���������� ������� � ������������� ������ ����������");
	puts("(����� ��������� ���������� � �������� ����.2 ���� ����������):");
	puts("\t\t\t\t\t\t\t\t\t\t\t       ������� 4");
	puts("---------------------------------------------------------------------------------------------------------");
	puts("    Z\t\t     X\t\t     Y\t\t Vwe, m/s\t  Vsn, m/s\t Climb, m/s\tposition");
	puts("---------------------------------------------------------------------------------------------------------");
#else
	puts("---------------------------------------------------------------------------------------------------------");
#endif
	///
	// ...	��� ���������� ������������� �������� '���������� ����������'
	// 		������������ '������� �������' (event_log)
	for (int jsp = 0; jsp < 3; jsp++) {
		calculating_intermediate_values_speeds(jsp);
	}
	///
	for (int jsc = 0; jsc < 8; jsc++) {
#ifdef visualization_of_intermediate_calculations_when_debugging
		printf("%f\t %f\t %f\t %f\t %f\t %f\t   %d\n", dprm.loc[0][jsc], dprm.loc[1][jsc], dprm.loc[2][jsc], dprm.spd[0][jsc], dprm.spd[1][jsc], dprm.spd[2][jsc], jsc + 1);
#endif
	}
#ifdef visualization_of_intermediate_calculations_when_debugging
	puts("---------------------------------------------------------------------------------------------------------");
#endif
#ifdef visualization_of_intermediate_calculations_when_debugging
	puts("\n# ���������� ����������, ������� �������� � ���������� �������");
	puts("����� ������� ���������� ���������� � ������������:");
	puts("\t\t\t\t\t\t\t       ������� 5");
	puts("-------------------------------------------------------------------------");
	puts(" position   dDist, m\t  Vgr, m/s\t   dt, s\t   t, s");
	puts("-------------------------------------------------------------------------");
#endif
	///
	distance_calculation_plus(8, 0);
	///
#ifdef visualization_of_intermediate_calculations_when_debugging
	puts("-------------------------------------------------------------------------");
	puts("\n# ���������� ����������, ������� �������� � ���������� �������");
	puts("����� ������� ���������� ���������� � ��������� XZ:");
	puts("\t\t\t\t\t\t\t      ������� 5a");
	puts("-------------------------------------------------------------------------");
	puts(" position   dDist, m\t  Vxz, m/s\t   dt, s\t   t, s");
	puts("-------------------------------------------------------------------------");
#endif
	///
	distance_calculation_plus(8, 1);
	///
#ifdef visualization_of_intermediate_calculations_when_debugging
	puts("-------------------------------------------------------------------------");
#endif
	///
	int ja = 1;
	 dprm.acc[0][ja - 1] = 2 * (dprm.dist[1][ja + 1] - (dprm.Vgr[1][ja] * dprm.t[1][ja + 1])) / pow(dprm.t[1][ja + 1], 2);
#ifdef visualization_of_intermediate_calculations_when_debugging
		printf("%f\t %f\t %f\t %f\t %f\n", dprm.acc[0][ja], dprm.dist[1][ja], dprm.Vgr[1][ja], dprm.t[1][ja], dprm.Vgr[1][ja - 1] * dprm.t[1][ja]);
#endif
	///


#ifndef visualization_of_intermediate_calculations_when_debugging_extrapolation
	puts("# ������������� �������� ������ ���������� �����:");
#endif
	// ...	������������� �������� ������ ���������� �����
	//		� ������ ��������� (�����������������) ���������� ��������� ��� ������ ����������
	initialization_of_flight_path_source_data(0, positions - 1, p0.time_tsc);
	///
	int module;
	for (module = 3; module < 4; module++) {
		// ...	���������� ������������� ������������� (� ������������� ������� ���������)
		module = calculation_of_approximation_coefficients(0, 5, module);
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
		printf("st: %f\t st2: %f\t st3: %f\t st4: %f\n", dde.st[module], dde.rst2[module], dde.rst3[module], dde.rst4[module]);
		printf("sP: %f\t stP: %f\t sPt2: %f\n", dde.sP[module], dde.stP[module], dde.sPrt2[module]);
#endif
		// ...	���������� ������� �� ������������ � ���������� ������� ��������
		module = determinant_matrix(5, module, false);
#ifdef visualization_of_intermediate_calculations_when_debugging_extrapolation
        printf("approx: %d\t R1: %f\t R2: %f\t\t R3: %f\n", module, dMat.R1[module], dMat.R2[module], dMat.R3[module]);
#endif
	}

	puts("\nHello World!");
	return EXIT_SUCCESS;
}
